package edu.dhbwka.gtris.presentation.view;

import edu.dhbwka.gtris.model.application.IGameConfiguration;
import edu.dhbwka.gtris.presentation.view.component.uimodel.IUIConfig;
import edu.dhbwka.gtris.presentation.view.component.uimodel.Localizable;
import edu.dhbwka.gtris.presentation.view.component.uimodel.Localization;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import javax.swing.JFrame;
import javax.swing.JRootPane;


class GameFrame extends JFrame {
    private final HashMap<Class, Container> views = new HashMap<>();
    private final IGameConfiguration gameConfiguration;
    private final GameUI gameUIInstance;

    GameFrame(GameUI gameUI, Class view) {
        this(gameUI);
        setContentPane(views.get(view));
    }

    GameFrame(GameUI gameUI) {
        super(gameUI.getUiConfig().getWindowTitle());
        gameUIInstance = gameUI;
        gameConfiguration = gameUI.getGameConfiguration();

        addView(GameView.class, new GameView(this, gameConfiguration));
        addView(ControlsInfoPanel.class, new ControlsInfoPanel(this));
        addView(SettingsMenuPanel.class, new SettingsMenuPanel(this));
        addView(HighScoresPanel.class,
                new HighScoresPanel(this, gameConfiguration.getHighScoreRepository()));
        addView(MainMenuPanel.class, new MainMenuPanel(this));

        final JRootPane rootPane1 = getRootPane();
        final IUIConfig uiConfig = getUiConfig();
        final Color commonBackground = uiConfig.getCommonBackground();
        rootPane1.setBackground(commonBackground);
        setBackground(commonBackground);
        setMinimumSize(
                new Dimension(uiConfig.getWindowMinWidth(), uiConfig.getWindowMinHeight()));
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setContentPane(views.get(MainMenuPanel.class));
        pack();
        setVisible(true);
    }

    @Override public void setContentPane(Container contentPane) {
        super.setContentPane(contentPane);
        revalidate();
        repaint();
    }

    private void addView(Class viewClass, Container instance) {
        views.put(viewClass, instance);
    }

    public IUIConfig getUiConfig() {
        return gameUIInstance.getUiConfig();
    }

    public GameView getGameView() {
        return (GameView) views.get(GameView.class);
    }

    public GameView getGameView(boolean newGame) {
        if (newGame) {
            views.remove(GameView.class);
            views.put(GameView.class, new GameView(this, gameConfiguration));
            ((GameView) views.get(GameView.class)).resume();
        }
        return (GameView) views.get(GameView.class);
    }

    public ControlsInfoPanel getControlsInfoPanel() {
        return (ControlsInfoPanel) views.get(ControlsInfoPanel.class);
    }

    public SettingsMenuPanel getSettingsMenuPanel() {
        return (SettingsMenuPanel) views.get(SettingsMenuPanel.class);
    }

    public HighScoresPanel getHighScoresPanel() {
        ((HighScoresPanel) views.get(HighScoresPanel.class)).reload();
        return (HighScoresPanel) views.get(HighScoresPanel.class);
    }

    public MainMenuPanel getMainMenuPanel() {
        return (MainMenuPanel) views.get(MainMenuPanel.class);
    }

    public String localize(Localizable localizable) {
        final Localization localization = gameUIInstance.getLocalization();
        return localization.text(localizable);
    }

    public void toggleMusic() {
        gameUIInstance.toggleMusic();
    }

    public int getMusicVolume() {
        return gameUIInstance.getMusicVolume();
    }

    public void setMusicVolume(int volume) {
        gameUIInstance.setMusicVolume(volume);
    }

    public List<String> getAvailableLanguages() {
        final Localization localization = gameUIInstance.getLocalization();
        final List<Locale> availableLanguages = localization.getAvailableLanguages();
        return availableLanguages.stream().map(Locale::getDisplayName).collect(Collectors.toList());
    }

    public void switchLanguage(Class lastClass) {
        gameUIInstance.switchLanguage(lastClass);
    }

    public void exit() {
        gameUIInstance.leave();
    }
}
