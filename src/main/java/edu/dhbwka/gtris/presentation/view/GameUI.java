package edu.dhbwka.gtris.presentation.view;

import com.formdev.flatlaf.FlatDarkLaf;
import com.formdev.flatlaf.FlatLaf;
import com.formdev.flatlaf.fonts.roboto.FlatRobotoFont;
import edu.dhbwka.gtris.model.application.IGameConfiguration;
import edu.dhbwka.gtris.presentation.view.component.uimodel.GtrisMusic;
import edu.dhbwka.gtris.presentation.view.component.uimodel.IUIConfig;
import edu.dhbwka.gtris.presentation.view.component.uimodel.Localization;
import java.awt.Point;
import java.util.List;
import java.util.Locale;

public class GameUI {
    private final IUIConfig uiConfig;
    private final IGameConfiguration gameConfiguration;
    private final Localization localization;
    private final GtrisMusic gtrisMusic;
    private GameFrame gameFrame;

    public GameUI(IUIConfig uiConfig, IGameConfiguration gameConfiguration) {
        this(uiConfig, gameConfiguration, uiConfig.getDefaultLocale());
        if (localization.isAvailable(Locale.getDefault())) {
            setLanguage(Locale.getDefault());
        }
    }

    private GameUI(IUIConfig uiConfig, IGameConfiguration gameConfiguration,
            Locale locale) {
        this.uiConfig = uiConfig;
        this.gameConfiguration = gameConfiguration;
        this.localization = new Localization(locale);
        gtrisMusic = GtrisMusic.getInstance(this.uiConfig.getMusicThemefile(),
                this.uiConfig.getMusicSoundFont(), this.uiConfig.getMusicDefaultVolume());
        FlatRobotoFont.install();
        FlatLaf.setPreferredFontFamily(FlatRobotoFont.FAMILY);
        FlatLaf.setPreferredLightFontFamily(FlatRobotoFont.FAMILY_LIGHT);
        FlatLaf.setPreferredSemiboldFontFamily(FlatRobotoFont.FAMILY_SEMIBOLD);
        FlatDarkLaf.setup();

        gameFrame = new GameFrame(this);
    }

    private void setLanguage(Locale locale) {
        localization.setLanguage(locale);
        Locale.setDefault(locale);
    }

    public IGameConfiguration getGameConfiguration() {
        return gameConfiguration;
    }

    public int getMusicVolume() {
        return gtrisMusic.getVolume();
    }

    public void setMusicVolume(int volume) {
        gtrisMusic.setVolume(volume);
    }

    public void toggleMusic() {
        if (gtrisMusic.isMusicRunning()) {
            gtrisMusic.stop();
        } else {
            gtrisMusic.start();
        }
    }

    public void switchLanguage(Class lastClass) {
        List<Locale> translations = localization.getAvailableLanguages();
        int index = translations.indexOf(localization.getLocale());
        index = (index + 1) % translations.size();
        Locale locale = translations.get(index);
        setLanguage(locale);
        reloadGameFrame(lastClass);
    }

    private void reloadGameFrame(Class lastClass) {
        Point location = gameFrame.getLocation();
        gameFrame.dispose();
        gameFrame = new GameFrame(this, lastClass);
        gameFrame.setLocation(location);
    }

    public Localization getLocalization() {
        return localization;
    }

    public IUIConfig getUiConfig() {
        return uiConfig;
    }

    public void leave() {
        gtrisMusic.stop();
        gameFrame.dispose();
        System.exit(0);
    }
}
