package edu.dhbwka.gtris.presentation.view;

import edu.dhbwka.gtris.presentation.view.component.StyledPanel;
import edu.dhbwka.gtris.presentation.view.component.uimodel.Localizable;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class OverlayPanel extends StyledPanel {
    private final JLabel prompt;
    private final JTextField txtPlayerName;
    private final JButton quitButton;
    private final GameFrame gameFrame;
    private final Type type;
    private final ActionListener saveActionListener = new SaveActionListener();
    private boolean saveDisabled = true;

    public OverlayPanel(Type type, GameFrame gameFrame) {
        super(gameFrame.getUiConfig());
        this.gameFrame = gameFrame;
        this.type = type;

        prompt = styleLabel(new JLabel(gameFrame.localize(Localizable.LABEL_YOURNAME)));
        txtPlayerName = styleTextField(new JTextField());
        final JButton saveGameButton =
                styleButton(new JButton(gameFrame.localize(Localizable.BUTTON_SAVEGAME)));
        quitButton =
                styleButton(new JButton(gameFrame.localize(Localizable.BUTTON_QUIT)));

        setBackground(gameFrame.getUiConfig().getStatuswindowBackground());
        setForeground(gameFrame.getUiConfig().getStatuswindowForeground());
        setBorder(gameFrame.getUiConfig().getStatuswindowBorder());

        setLayout(
                new GridLayout(0, 1, gameFrame.getUiConfig().getStatuswindowComponentPadding(),
                        gameFrame.getUiConfig().getStatuswindowComponentPadding()));
        int width = gameFrame.getSize().width / 2;
        int height = gameFrame.getSize().height / 2;
        setOpaque(true);
        setMaximumSize(new Dimension(width, height));

        JLabel heading = new JLabel(gameFrame.localize(type.heading), SwingConstants.CENTER);
        heading.setFont(gameFrame.getUiConfig().getStatuswindowHeadingFont());
        add(heading);
        if (Type.PAUSE == type) {
            add(saveGameButton);
            saveGameButton.addActionListener(a -> gameFrame.getGameView().saveGame());
            final JButton resumeButton =
                    styleButton(new JButton(gameFrame.localize(Localizable.BUTTON_RESUME)));
            add(resumeButton);
            resumeButton.addActionListener(e -> gameFrame.getGameView().resume());
        }

        if (Type.PAUSE == type || Type.GAMEOVER == type) {
            quitButton.addActionListener(
                    e -> gameFrame.setContentPane(gameFrame.getMainMenuPanel()));
            add(quitButton);
        }
    }

    public JTextField getTxtPlayerName() {
        return txtPlayerName;
    }

    public void enableSaveFunction(boolean isHighScore) {
        if (saveDisabled && isHighScore && (Type.GAMEOVER == this.type)) {
            remove(quitButton);
            add(prompt);
            add(txtPlayerName);
            add(quitButton);
            quitButton.addActionListener(saveActionListener);
            saveDisabled = false;
        }
    }

    public enum Type {
        PAUSE(Localizable.LABEL_PAUSED),
        GAMEOVER(Localizable.LABEL_GAMEOVER),
        COUNTDOWN(Localizable.LABEL_COUNTDOWN);

        final Localizable heading;

        Type(Localizable localizable) {
            heading = localizable;
        }
    }

    private class SaveActionListener implements ActionListener {
        @Override public void actionPerformed(ActionEvent actionEvent) {
            String playerName = txtPlayerName.getText().trim();
            if (playerName.isEmpty()) {
                playerName = "--";
            }
            gameFrame.getGameView().saveHighScore(playerName);

        }
    }
}
