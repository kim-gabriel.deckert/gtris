package edu.dhbwka.gtris.presentation.view.component;

import com.github.weisj.jsvg.SVGDocument;
import com.github.weisj.jsvg.attributes.ViewBox;
import com.github.weisj.jsvg.parser.SVGLoader;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.util.Objects;
import javax.swing.JComponent;

public class SvgImageComponent extends JComponent {
    private final SVGDocument svgDocument;

    public SvgImageComponent(String svgResource) {
        final SVGLoader svgLoader = new SVGLoader();
        ClassLoader classLoader = getClass().getClassLoader();
        svgDocument = svgLoader.load(
                Objects.requireNonNull(classLoader.getResourceAsStream(svgResource)));
    }

    @Override protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_STROKE_CONTROL,
                RenderingHints.VALUE_STROKE_PURE);

        svgDocument.render(this, (Graphics2D) g,
                new ViewBox(0, 0, getSize().width, getSize().height));
    }
}
