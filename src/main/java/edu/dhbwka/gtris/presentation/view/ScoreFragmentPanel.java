package edu.dhbwka.gtris.presentation.view;

import edu.dhbwka.gtris.presentation.view.component.StyledPanel;
import edu.dhbwka.gtris.presentation.view.component.uimodel.IUIConfig;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JTextField;

class ScoreFragmentPanel extends FragmentPanel {
    private final JTextField scoreText = styleTextField(new JTextField(""));
    private final JTextField levelText = styleTextField(new JTextField(""));
    private final JTextField rowsText = styleTextField(new JTextField(""));

    ScoreFragmentPanel(IUIConfig uiConfig, String labelScore, String labelLevel,
            String labelRows) {
        super(uiConfig, "");
        final JLabel scoreLabel = styleLabel(new JLabel(labelScore));
        final JLabel levelLabel = styleLabel(new JLabel(labelLevel));
        final JLabel rowsLabel = styleLabel(new JLabel(labelRows));
        final StyledPanel contentPanel = new StyledPanel(uiConfig);

        for (JLabel label : new JLabel[]{ scoreLabel, levelLabel, rowsLabel }) {
            label.setForeground(uiConfig.getFragmentTitleForeground());
            label.setFont(uiConfig.getFragmentTitleFont());
        }

        for (JTextField textField : new JTextField[]{ scoreText, levelText,
                rowsText }) {
            textField.setFont(uiConfig.getScorelabelFont());
            textField.setBackground(uiConfig.getScorelabelBackground());
            textField.setForeground(uiConfig.getScorelabelForeground());
            textField.setEnabled(false);
        }

        contentPanel.setLayout(new GridLayout(6, 1));
        contentPanel.add(scoreLabel);
        contentPanel.add(scoreText);
        contentPanel.add(levelLabel);
        contentPanel.add(levelText);
        contentPanel.add(rowsLabel);
        contentPanel.add(rowsText);

        setScore(0);
        setLevel(1);
        setNumRows(0);

        add(contentPanel, BorderLayout.CENTER);
    }

    public void setScore(int score) {
        scoreText.setText(String.valueOf(score));
    }

    public void setLevel(int level) {
        levelText.setText(String.valueOf(level));
    }

    public void setNumRows(int numRows) {
        rowsText.setText(String.valueOf(numRows));
    }
}
