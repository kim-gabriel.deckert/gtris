package edu.dhbwka.gtris.presentation.view;

import edu.dhbwka.gtris.model.GameController;
import edu.dhbwka.gtris.model.IGameController;
import edu.dhbwka.gtris.model.application.IGameConfiguration;
import edu.dhbwka.gtris.model.application.entities.GTromino;
import edu.dhbwka.gtris.model.application.entities.IMino;
import edu.dhbwka.gtris.model.application.entities.IReadableScore;
import edu.dhbwka.gtris.presentation.view.component.PlayfieldGridComponent;
import edu.dhbwka.gtris.presentation.view.component.StyledPanel;
import edu.dhbwka.gtris.presentation.view.component.uimodel.IUIConfig;
import edu.dhbwka.gtris.presentation.view.component.uimodel.Localizable;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.KeyboardFocusManager;
import java.awt.LayoutManager;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.util.HashMap;
import java.util.List;
import javax.swing.JLayeredPane;
import javax.swing.JTextField;
import javax.swing.OverlayLayout;
import javax.swing.Timer;

public class GameView extends JLayeredPane {

    private final KeyControl gameControls = new KeyControl();
    private final Timer gameLoopTimer;
    private final IGameController gameController;
    private final IUIConfig uiConfig;
    private final GameFrame gameFrame;
    private final PlayfieldGridComponent playfieldGridComponent;
    private final HashMap<OverlayPanel.Type, OverlayPanel> overlays = new HashMap<>();
    private HoldboxFragmentPanel holdboxFragment;
    private ScoreFragmentPanel scoreFragment;
    private PreviewFragmentPanel previewFragment;
    private StyledPanel gamePanel;

    public GameView(GameFrame gameFrame, IGameConfiguration gameConfiguration) {
        this.gameFrame = gameFrame;
        this.uiConfig = gameFrame.getUiConfig();
        LayoutManager overlayLayout = new OverlayLayout(this);
        setLayout(overlayLayout);

        overlays.put(OverlayPanel.Type.COUNTDOWN, new CountDownOverlayPanel(gameFrame));
        overlays.put(OverlayPanel.Type.GAMEOVER,
                new OverlayPanel(OverlayPanel.Type.GAMEOVER, gameFrame));
        overlays.put(OverlayPanel.Type.PAUSE,
                new OverlayPanel(OverlayPanel.Type.PAUSE, gameFrame));

        gameController = new GameController(gameConfiguration);

        IGameConfiguration gameConfiguration1 = gameController.getGameConfiguration();
        int playfieldWidth = gameConfiguration1.getPlayfieldWidth();
        int playfieldHeight = gameConfiguration1.getPlayfieldHeight();
        playfieldGridComponent =
                new PlayfieldGridComponent(playfieldWidth, playfieldHeight, uiConfig);

        gameLoopTimer = new Timer(uiConfig.getViewUpdateTimer(), e -> update());
        gameLoopTimer.setInitialDelay(0);
        gameLoopTimer.setRepeats(true);

        initComponents();

        int index = 0;
        add(gamePanel, index);
        index++;

        for (OverlayPanel overlayPanel : overlays.values()) {
            overlayPanel.setVisible(false);
            add(overlayPanel, index);
            index++;
        }
    }

    private void update() {
        gameController.update();
        if (gameController.isPaused() || gameController.isGameOver()) {
            gameLoopTimer.stop();
            ((CountDownOverlayPanel) overlays.get(OverlayPanel.Type.COUNTDOWN)).stop();
            gameControls.getGameController(null);
            KeyboardFocusManager.getCurrentKeyboardFocusManager()
                    .removeKeyEventDispatcher(gameControls);
        }
        KeyboardFocusManager.getCurrentKeyboardFocusManager().clearFocusOwner();
        if (gameController.isPaused()) {
            OverlayPanel overlayPanel = overlays.get(OverlayPanel.Type.PAUSE);
            placeOnTop(overlayPanel);
            JTextField txtPlayerName = overlayPanel.getTxtPlayerName();
            txtPlayerName.requestFocus();
        }
        if (gameController.isGameOver()) {
            OverlayPanel overlayPanel = overlays.get(OverlayPanel.Type.GAMEOVER);
            placeOnTop(overlayPanel);
            JTextField txtPlayerName = overlayPanel.getTxtPlayerName();
            txtPlayerName.requestFocus();
        }
        render();
    }

    private void initComponents() {
        gamePanel = new StyledPanel(this.uiConfig);
        StyledPanel leftPanel = new StyledPanel(this.uiConfig);
        StyledPanel middlePanel = new StyledPanel(this.uiConfig);
        StyledPanel rightPanel = new StyledPanel(this.uiConfig);

        leftPanel.setMinimumSize(new Dimension(120, 600));
        leftPanel.setMaximumSize(new Dimension(1000, 5000));
        leftPanel.setPreferredSize(new Dimension(200, 1000));
        leftPanel.setAlignmentY(1.0F);
        leftPanel.setAlignmentX(0.0F);
        leftPanel.setLayout(new GridLayout(2, 1, uiConfig.getFragmentPadding(),
                uiConfig.getFragmentPadding()));
        holdboxFragment = (HoldboxFragmentPanel) styleFragment(
                new HoldboxFragmentPanel(uiConfig,
                        gameFrame.localize(Localizable.LABEL_HOLD)));
        leftPanel.add(holdboxFragment);

        scoreFragment = (ScoreFragmentPanel) styleFragment(
                new ScoreFragmentPanel(uiConfig,
                        gameFrame.localize(Localizable.LABEL_SCORE),
                        gameFrame.localize(Localizable.LABEL_LEVEL),
                        gameFrame.localize(Localizable.LABEL_ROWS)));
        leftPanel.add(scoreFragment);

        middlePanel.setMinimumSize(new Dimension(300, 600));
        middlePanel.setMaximumSize(new Dimension(2500, 5000));
        middlePanel.setPreferredSize(new Dimension(500, 1000));
        middlePanel.setAlignmentY(1.0F);
        middlePanel.setLayout(new GridLayout(1, 1));
        middlePanel.addComponentListener(new ComponentAdapter() {
            @Override public void componentResized(ComponentEvent e) {
                playfieldGridComponent.resize();
            }
        });
        playfieldGridComponent.resize();
        middlePanel.add(playfieldGridComponent);

        rightPanel.setMinimumSize(new Dimension(120, 600));
        rightPanel.setMaximumSize(new Dimension(1000, 5000));
        rightPanel.setPreferredSize(new Dimension(200, 1000));
        rightPanel.setAlignmentY(1.0F);
        rightPanel.setAlignmentX(1.0F);
        rightPanel.setLayout(new GridLayout(1, 1, uiConfig.getFragmentPadding(),
                uiConfig.getFragmentPadding()));
        int previewCount = gameController.getGameConfiguration().getPreviewCount();
        previewFragment = (PreviewFragmentPanel) styleFragment(
                new PreviewFragmentPanel(uiConfig,
                        gameFrame.localize(Localizable.LABEL_NEXT), previewCount));
        rightPanel.add(previewFragment);

        gamePanel.setMinimumSize(new Dimension(540, 600));
        gamePanel.setPreferredSize(new Dimension(900, 1000));
        gamePanel.setLayout(new GridBagLayout());
        Insets insets =
                new Insets(uiConfig.getFragmentPadding(), uiConfig.getFragmentPadding(),
                        uiConfig.getFragmentPadding(), uiConfig.getFragmentPadding());
        gamePanel.add(leftPanel,
                new GridBagConstraints(0, 0, 1, 1, 0.2, 1.0, GridBagConstraints.LINE_START,
                        GridBagConstraints.BOTH, insets, 0, 0));
        gamePanel.add(middlePanel,
                new GridBagConstraints(1, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER,
                        GridBagConstraints.BOTH, insets, 0, 0));
        gamePanel.add(rightPanel,
                new GridBagConstraints(2, 0, 1, 1, 0.2, 1.0, GridBagConstraints.LINE_END,
                        GridBagConstraints.BOTH, insets, 0, 0));
    }

    private void placeOnTop(Container container) {
        for (OverlayPanel overlayPanel : overlays.values()) {
            overlayPanel.setVisible(false);
            int score = this.gameController.getScore().getScore();
            overlayPanel.enableSaveFunction(0 < score);
        }
        container.setVisible(true);
        moveToFront(container);
    }

    private void render() {
        List<IMino> minos = gameController.getAllMinos();
        List<IMino> ghostMinos = gameController.getGhostMinos();
        playfieldGridComponent.renderMinos(minos, ghostMinos);
        playfieldGridComponent.resize();

        holdboxFragment.setGtromino(gameController.getHoldGtrominoType());
        holdboxFragment.resize();

        int previewCount = gameController.getGameConfiguration().getPreviewCount();
        GTromino[] previewGTrominos = gameController.getPreviewGtrominos(previewCount);
        previewFragment.setGtrominoTypes(previewGTrominos);
        previewFragment.resize();

        IReadableScore score = gameController.getScore();
        scoreFragment.setScore(score.getScore());
        scoreFragment.setLevel(score.getLevel());
        scoreFragment.setNumRows(score.getRows());
    }

    private FragmentPanel styleFragment(FragmentPanel fragmentPanel) {
        fragmentPanel.setBorder(uiConfig.getFragmentBorder());
        return fragmentPanel;
    }

    public void saveGame() {
        gameController.saveGameState();
    }

    public void loadGame() {
        gameController.loadGameState();
        resume();
    }

    public void resume() {
        placeOnTop(overlays.get(OverlayPanel.Type.COUNTDOWN));
        ((CountDownOverlayPanel) overlays.get(OverlayPanel.Type.COUNTDOWN)).start(() -> {
            placeOnTop(gamePanel);
            gameController.start();
            gameLoopTimer.start();
            gameControls.getGameController(gameController);
            KeyboardFocusManager.getCurrentKeyboardFocusManager()
                    .addKeyEventDispatcher(gameControls);
        });
    }

    public void saveHighScore(String playerName) {
        gameController.saveHighScore(playerName);
    }
}
