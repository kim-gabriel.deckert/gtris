package edu.dhbwka.gtris.presentation.view;

import edu.dhbwka.gtris.presentation.view.component.StyledPanel;
import edu.dhbwka.gtris.presentation.view.component.uimodel.IUIConfig;
import java.awt.BorderLayout;
import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

class FragmentPanel extends StyledPanel {

    FragmentPanel(IUIConfig uiConfig, String heading) {
        super(uiConfig);
        final JLabel fragmentHeading = new JLabel(heading);
        fragmentHeading.setHorizontalAlignment(SwingConstants.CENTER);
        fragmentHeading.setAlignmentX(CENTER_ALIGNMENT);
        fragmentHeading.setForeground(uiConfig.getFragmentTitleForeground());
        fragmentHeading.setFont(uiConfig.getFragmentTitleFont());
        setLayout(new BorderLayout());
        add(fragmentHeading, BorderLayout.PAGE_START);
    }
}
