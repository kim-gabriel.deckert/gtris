package edu.dhbwka.gtris.presentation.view.component;

import edu.dhbwka.gtris.model.application.entities.GTromino;
import edu.dhbwka.gtris.model.application.entities.IMino;
import edu.dhbwka.gtris.model.application.entities.primitives.Position;
import edu.dhbwka.gtris.presentation.view.component.uimodel.IUIConfig;
import java.awt.BorderLayout;
import java.util.Arrays;
import java.util.Objects;
import javax.swing.BorderFactory;
import javax.swing.JComponent;

public class GtrominoComponent extends JComponent {
    private final IUIConfig uiConfig;
    private final StyledPanel gtrominoPanel;
    private MinoComponent[][] minoComponents;
    private GTromino gtromino;
    private int minoSize;

    public GtrominoComponent(final IUIConfig uiConfig) {
        this.uiConfig = uiConfig;
        this.setLayout(new BorderLayout());
        this.gtrominoPanel = new StyledPanel(this.uiConfig);
        this.add(this.gtrominoPanel, BorderLayout.CENTER);
        this.setOpaque(false);
        this.setFocusable(false);
        this.setAlignmentX(0.5f);
        this.setAlignmentY(0.5f);
    }

    public void setGtromino(GTromino gtromino) {
        if (null != gtromino && (!gtromino.equals(this.gtromino))) {
            this.gtromino = gtromino;
            final int viewDimension = this.gtromino.getDimension();
            this.gtrominoPanel.removeAll();
            this.gtrominoPanel.setLayout(new MinoMatrixLayout(viewDimension, viewDimension,
                    this.uiConfig.getMinimalMinosize(), this.gtrominoPanel));
            this.minoComponents = new MinoComponent[viewDimension][viewDimension];
            for (int x = 0; x < viewDimension; x++) {
                for (int y = 0; y < viewDimension; y++) {
                    this.minoComponents[x][y] = MinoComponent.createMinoComponent(this.uiConfig,
                            this.minoSize,
                            new Position(x, y));
                    this.gtrominoPanel.add(this.minoComponents[x][y], new Position(x, y));
                }
            }
            final IMino[][] minos = this.gtromino.createMinos();
            final int[] rgba = gtromino.getColor().getRgba();
            for (int x = 0; x < viewDimension; x++) {
                for (int y = 0; y < viewDimension; y++) {
                    this.minoComponents[x][y].setMinoSize(this.minoSize);
                    if (null != minos[x][y]) {
                        this.minoComponents[x][y].setAttributes(false, rgba[0], rgba[1], rgba[2],
                                rgba[3]);
                        this.minoComponents[x][y].repaint();
                    }
                }
            }
        }
        this.revalidate();
        this.repaint();
    }

    public void resize() {
        final int minoWidth =
                (null != gtromino ? this.getSize().width / gtromino.getDimension() : 1);
        final int minoHeight =
                (null != gtromino ? this.getSize().height / gtromino.getDimension() : 1);
        final int componentSize = Math.min(minoWidth, minoHeight);
        if (null != this.minoComponents) {
            minoSize = componentSize;
            Arrays.stream(this.minoComponents).flatMap(Arrays::stream).filter(Objects::nonNull)
                    .forEach(mv -> mv.setMinoSize(componentSize));
        }
        final int paddingX = (int) Math.floor(
                (minoWidth / this.uiConfig.getGtrominoComponentPaddingXDivisor()));
        final int paddingY = (int) Math.floor(
                (minoWidth / this.uiConfig.getGtrominoComponentPaddingYDivisor()));
        this.setBorder(BorderFactory.createEmptyBorder(paddingY, paddingX, paddingY, paddingX));
        this.repaint();
    }
}
