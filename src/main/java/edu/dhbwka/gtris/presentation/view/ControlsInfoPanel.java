package edu.dhbwka.gtris.presentation.view;

import edu.dhbwka.gtris.presentation.view.component.StyledPanel;
import edu.dhbwka.gtris.presentation.view.component.uimodel.GameControls;
import edu.dhbwka.gtris.presentation.view.component.uimodel.Localizable;
import java.awt.BorderLayout;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class ControlsInfoPanel extends StyledPanel {

    private final GameFrame gameFrame;

    ControlsInfoPanel(GameFrame gameFrame) {
        super(gameFrame.getUiConfig());
        this.gameFrame = gameFrame;
        this.setLayout(new BorderLayout());
        DefaultTableModel model = new DefaultTableModel(this.generateTableData(),
                new String[]{ gameFrame.localize(Localizable.LABEL_KEYDESCRIPTION),
                        gameFrame.localize(Localizable.LABEL_KEYS) });
        JTable controlsTable = this.styleTable(new JTable(model));
        this.add(new JScrollPane(controlsTable), BorderLayout.CENTER);
        JButton backButton = this.styleButton(new JButton(gameFrame.localize(Localizable.BUTTON_BACK)));
        backButton.addActionListener(e -> gameFrame.setContentPane(gameFrame.getMainMenuPanel()));
        this.add(backButton, BorderLayout.PAGE_END);
    }

    private String[][] generateTableData() {
        final List<List<String>> controlsDescriptions = new ArrayList<>();
        Stream<GameControls.GameAction> stream =
                Arrays.stream(GameControls.GameAction.values());
        stream.forEach(action -> {
            final List<String> controlsDescription = new ArrayList<>();
            final Collection<String> keyCodesDescription = new ArrayList<>();
            final Set<Map.Entry<Integer, GameControls.GameAction>> entries =
                    GameControls.GameActionKeyCodes.entrySet();
            entries.stream().filter(actionKeyCode -> actionKeyCode.getValue() == action)
                    .map(Map.Entry::getKey)
                    .forEach(keyCode -> keyCodesDescription.add(KeyEvent.getKeyText(keyCode)));
            controlsDescription.add(
                    gameFrame.localize(GameControls.getGameActionLocalizable(action)));
            controlsDescription.add(String.join(", ", keyCodesDescription));
            controlsDescriptions.add(controlsDescription);
        });
        final String[][] tableData = new String[controlsDescriptions.size()][2];
        controlsDescriptions.forEach(row -> row.forEach(column -> {
            int i = controlsDescriptions.indexOf(row);
            int j = row.indexOf(column);
            tableData[i][j] = column;
        }));
        return tableData;
    }
}
