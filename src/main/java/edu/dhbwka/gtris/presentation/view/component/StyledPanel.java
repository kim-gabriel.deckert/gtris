package edu.dhbwka.gtris.presentation.view.component;

import edu.dhbwka.gtris.presentation.view.component.uimodel.IUIConfig;
import java.awt.BorderLayout;
import java.awt.Component;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;

public class StyledPanel extends JPanel {
    private final IUIConfig uiConfig;

    public StyledPanel(IUIConfig uiConfig) {
        this(uiConfig, true, null);
    }

    private StyledPanel(IUIConfig uiConfig, boolean minimal, String headingText) {
        this.uiConfig = uiConfig;
        if (!minimal) {
            setLayout(new BorderLayout());
            JLabel headingLabel = new JLabel(headingText);
            add(headingLabel, BorderLayout.PAGE_START);
        }
        setOpaque(false);
        setRequestFocusEnabled(false);
        setFocusable(false);
    }

    protected JTable styleTable(JTable table) {
        table.setEnabled(false);
        table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        DefaultTableCellRenderer cellRenderer = new DefaultTableCellRenderer();
        table.setBackground(uiConfig.getTableBackground());
        table.setForeground(uiConfig.getTableForeground());
        table.setFont(uiConfig.getTableFont());
        cellRenderer.setFont(uiConfig.getTableFont());
        cellRenderer.setBackground(uiConfig.getTableBackground());
        cellRenderer.setForeground(uiConfig.getTableForeground());
        table.getColumnModel().getColumns().asIterator()
                .forEachRemaining(column -> column.setCellRenderer(cellRenderer));
        table.getTableHeader().setBackground(uiConfig.getTableHeaderBackground());
        table.getTableHeader().setForeground(uiConfig.getTableHeaderForeground());
        table.getTableHeader().setFont(uiConfig.getTableHeaderFont());
        table.setRowHeight(uiConfig.getTableRowHeight());
        return table;
    }

    protected JButton styleButton(JButton button) {
        button.setHorizontalAlignment(SwingConstants.CENTER);
        button.setAlignmentX(Component.CENTER_ALIGNMENT);
        button.setFont(uiConfig.getButtonFont());
        button.setBackground(uiConfig.getButtonBackground());
        button.setForeground(uiConfig.getButtonForeground());
        button.setPreferredSize(uiConfig.getButtonSize());
        return button;
    }

    protected JTextField styleTextField(JTextField textField) {
        textField.setHorizontalAlignment(SwingConstants.CENTER);
        textField.setAlignmentX(CENTER_ALIGNMENT);
        textField.setFont(uiConfig.getTextfieldFont());
        textField.setBackground(uiConfig.getTextfieldBackground());
        textField.setForeground(uiConfig.getTextfieldForeground());
        textField.setOpaque(true);
        return textField;
    }

    protected JButton styleMainMenuButton(JButton mainMenuButton) {
        mainMenuButton.setHorizontalAlignment(SwingConstants.CENTER);
        mainMenuButton.setAlignmentX(CENTER_ALIGNMENT);
        mainMenuButton.setBackground(uiConfig.getMainMenuButtonBackground());
        mainMenuButton.setForeground(uiConfig.getMainMenuButtonForeground());
        mainMenuButton.setFont(uiConfig.getMainMenuButtonFont());
        mainMenuButton.setSize(uiConfig.getMainMenuButtonWidth(),
                uiConfig.getMainMenuButtonHeight());
        mainMenuButton.setBorder(uiConfig.getMainMenuButtonBorder());
        return mainMenuButton;
    }

    protected JLabel styleSettingsMenuHeading(JLabel label) {
        label.setForeground(uiConfig.getSettingsMenuHeadingForeground());
        label.setFont(uiConfig.getSettingsMenuHeadingFont());
        label.setHorizontalAlignment(SwingConstants.CENTER);
        return label;
    }

    protected JComponent styleSettingsMenuButton(JComponent button) {
        button.setAlignmentX(CENTER_ALIGNMENT);
        button.setBackground(uiConfig.getSettingsMenuButtonBackground());
        button.setForeground(uiConfig.getSettingsMenuButtonForeground());
        button.setFont(uiConfig.getSettingsMenuButtonFont());
        button.setBorder(uiConfig.getSettingsMenuButtonBorder());
        return button;
    }

    protected JLabel styleLabel(JLabel label) {
        label.setHorizontalAlignment(SwingConstants.CENTER);
        label.setAlignmentX(CENTER_ALIGNMENT);
        label.setFont(uiConfig.getLabelFont());
        label.setBackground(uiConfig.getLabelBackground());
        label.setForeground(uiConfig.getLabelForeground());
        return label;
    }

}
