package edu.dhbwka.gtris.presentation.view.component.uimodel;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.DirectoryStream;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.Scanner;
import java.util.regex.Pattern;

public class Localization {

    private final String resourceBundleName = "Translations";
    private ResourceBundle resourceBundle;
    private Locale locale;

    public Localization(Locale locale) {
        resourceBundle =
                ResourceBundle.getBundle(resourceBundleName + File.separator + "Translation",
                        locale);
        this.locale = locale;
    }

    public Locale getLocale() {
        return locale;
    }

    public String text(Localizable localizable) {
        return resourceBundle.getString(localizable.name());
    }

    public boolean isAvailable(Locale locale) {
        List<Locale> availableLanguages = getAvailableLanguages();
        return availableLanguages.contains(locale);
    }

    public List<Locale> getAvailableLanguages() {
        Pattern langPattern = Pattern.compile("Translation_(\\w{1,3})\\.properties$");
        Collection<String> files = new ArrayList<>();
        URI uri;
        ClassLoader classLoader = getClass().getClassLoader();
        try {
            URL url =
                    Objects.requireNonNull(classLoader.getResource(resourceBundleName));
            uri = url.toURI();
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
        if ("jar".equals(uri.getScheme())) {
            try (FileSystem fileSystem = FileSystems.newFileSystem(uri,
                    new HashMap<String, Path>(), null)) {
                try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(
                        fileSystem.getPath(resourceBundleName))) {
                    for (Path directoryPath : directoryStream) {
                        files.add(directoryPath.getFileName().toString());
                    }
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        } else {
            try (final Scanner scanner = new Scanner(Objects.requireNonNull(
                    classLoader.getResourceAsStream(resourceBundleName)),
                    StandardCharsets.UTF_8)) {
                while (scanner.hasNextLine()) {
                    String file = scanner.nextLine();
                    files.add(file);
                }
            }
        }
        return files.stream().map(langPattern::matcher).map(matcher -> matcher.replaceAll("$1"))
                       .map(Locale::forLanguageTag).toList();
    }

    public void setLanguage(Locale locale) {
        this.locale = locale;
        resourceBundle =
                ResourceBundle.getBundle(resourceBundleName + File.separator + "Translation",
                        locale);
    }
}
