package edu.dhbwka.gtris.presentation.view;

import edu.dhbwka.gtris.presentation.view.component.StyledPanel;
import edu.dhbwka.gtris.presentation.view.component.SvgImageComponent;
import edu.dhbwka.gtris.presentation.view.component.uimodel.Localizable;
import java.awt.GridLayout;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;

public class MainMenuPanel extends StyledPanel {
    public MainMenuPanel(GameFrame gameFrame) {
        super(gameFrame.getUiConfig());

        JButton playButton =
                styleMainMenuButton(new JButton(gameFrame.localize(Localizable.BUTTON_PLAY)));
        playButton.addActionListener(e -> gameFrame.setContentPane(gameFrame.getGameView(true)));


        JButton loadGameButton = styleMainMenuButton(
                new JButton(gameFrame.localize(Localizable.BUTTON_LOADGAME)));
        loadGameButton.addActionListener(e -> {
            gameFrame.setContentPane(gameFrame.getGameView(true));
            gameFrame.getGameView().loadGame();
        });

        JButton highScoreListButton = styleMainMenuButton(
                new JButton(gameFrame.localize(Localizable.BUTTON_HIGHSCORETABLE)));
        highScoreListButton.addActionListener(
                e -> gameFrame.setContentPane(gameFrame.getHighScoresPanel()));

        JButton controlsButton = styleMainMenuButton(
                new JButton(gameFrame.localize(Localizable.BUTTON_CONTROLS)));
        controlsButton.addActionListener(
                e -> gameFrame.setContentPane(gameFrame.getControlsInfoPanel()));

        JButton settingsButton = styleMainMenuButton(
                new JButton(gameFrame.localize(Localizable.BUTTON_SETTINGS)));
        settingsButton.addActionListener(
                e -> gameFrame.setContentPane(gameFrame.getSettingsMenuPanel()));

        JButton quitButton =
                styleMainMenuButton(new JButton(gameFrame.localize(Localizable.BUTTON_QUIT)));
        quitButton.addActionListener(e -> gameFrame.exit());

        JPanel mainMenuButtonPanel = new StyledPanel(gameFrame.getUiConfig());
        JButton[] mainMenuButtons =
                { playButton, loadGameButton, highScoreListButton, controlsButton, settingsButton,
                        quitButton };


        int padding = gameFrame.getUiConfig().getMainMenuPadding();
        mainMenuButtonPanel.setLayout(
                new GridLayout((mainMenuButtons.length / 3) + 1, 3, padding, padding));
        mainMenuButtonPanel.setBorder(
                BorderFactory.createEmptyBorder(padding, padding, padding, padding));
        for (JButton mainMenuButton : mainMenuButtons) {
            mainMenuButtonPanel.add(mainMenuButton);
        }
        setLayout(new GridLayout(2, 1));

        SvgImageComponent svgImageComponent =
                new SvgImageComponent(gameFrame.getUiConfig().getMainMenuLogo());
        add(svgImageComponent);
        add(mainMenuButtonPanel);
    }
}
