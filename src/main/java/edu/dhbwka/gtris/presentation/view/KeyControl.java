package edu.dhbwka.gtris.presentation.view;

import edu.dhbwka.gtris.model.IGameController;
import edu.dhbwka.gtris.presentation.view.component.uimodel.GameControls;
import java.awt.KeyEventDispatcher;
import java.awt.event.KeyEvent;

class KeyControl implements KeyEventDispatcher {

    private IGameController gtrisController;

    KeyControl() {
    }

    @Override public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        final boolean result = false;
        if (null != gtrisController) {
            if (KeyEvent.KEY_PRESSED == keyEvent.getID()) {
                GameControls.GameAction requestedAction =
                        GameControls.getGameAction(keyEvent.getKeyCode());
                performOperation(requestedAction);
            }
        }

        return result;
    }

    private void performOperation(GameControls.GameAction action) {
        if (null != action) {
            switch (action) {
                case PAUSE -> gtrisController.pause();
                case SHIFT_LEFT -> gtrisController.shiftLeft();
                case SHIFT_RIGHT -> gtrisController.shiftRight();
                case ROTATE_CLOCKWISE -> gtrisController.rotateClockwise();
                case ROTATE_COUNTERCLOCKWISE -> gtrisController.rotateCounterClockwise();
                case DROP_HARD -> gtrisController.hardDrop();
                case DROP_SOFT -> gtrisController.softDrop();
                case HOLD -> gtrisController.hold();
            }
        }
    }

    public void getGameController(IGameController gtrisController) {
        this.gtrisController = gtrisController;
    }
}
