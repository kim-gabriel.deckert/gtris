package edu.dhbwka.gtris.presentation.view.component;

import edu.dhbwka.gtris.model.application.entities.primitives.Position;
import edu.dhbwka.gtris.presentation.view.component.uimodel.IUIConfig;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.Objects;
import javax.swing.JComponent;

class MinoComponent extends JComponent {
    private int minoSize;
    private Color color;
    private boolean ghost;
    private final Position position;
    private final IUIConfig uiConfig;

    private MinoComponent(final IUIConfig uiConfig, final int minoSize, final Position position) {
        this.minoSize = minoSize;
        setOpaque(false);
        setFocusable(false);
        this.uiConfig = uiConfig;
        this.position = position;
        this.ghost = false;
        this.color = null;
    }

    static MinoComponent createMinoComponent(final IUIConfig uiConfig, final int minoSize, final Position position) {
        return new MinoComponent(uiConfig, minoSize, position);
    }

    public int getState() {
        int result;
        if (null != color && null != this.position) {
            result = Objects.hash(Integer.valueOf(this.minoSize), Integer.valueOf(this.color.getRed()),
                    Integer.valueOf(this.color.getGreen()), Integer.valueOf(this.color.getBlue()),
                    Integer.valueOf(this.color.getAlpha()), Integer.valueOf(this.position.getX()),
                    Integer.valueOf(this.position.getY()), Boolean.valueOf(this.ghost));
        } else {
            result = Objects.hash(Integer.valueOf(this.minoSize), Boolean.valueOf(this.ghost));
        }
        return result;
    }

    void setAttributes(boolean isGhost, int r, int g, int b, int a) {
        ghost = isGhost;
        this.color = new Color(r, g, b, a);
    }

    public void reset() {
        this.ghost = false;
        this.color = null;
    }

    public Position getPosition() {
        return position;
    }

    @Override protected void paintComponent(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;
        final int borderRadiusX = (int) (minoSize / this.uiConfig.getMinoBorderRadiusXDivisor());
        final int borderRadiusY = (int) (this.minoSize / this.uiConfig.getMinoBorderRadiusYDivisor());
        if (null != color) {
            if (this.ghost) {
                g2d.setColor(new Color(this.color.getRed(), this.color.getGreen(), this.color.getBlue(), 127));
                g2d.fillRoundRect(0, 0, this.minoSize, this.minoSize, borderRadiusX, borderRadiusY);
                g2d.setColor(this.uiConfig.getGhostminoBorderColor());
                int borderThickness = this.uiConfig.getGhostminoBorderThickness();
                g2d.fillRoundRect(0, 0, this.minoSize, borderThickness, borderRadiusX, borderRadiusY);
                g2d.fillRoundRect(0, 0, borderThickness, this.minoSize, borderRadiusX, borderRadiusY);
                g2d.fillRoundRect(0, this.minoSize - borderThickness, this.minoSize, borderThickness,
                        borderRadiusX, borderRadiusY);
                g2d.fillRoundRect(this.minoSize - borderThickness, 0, borderThickness, this.minoSize,
                        borderRadiusX, borderRadiusY);
            } else {
                g2d.setColor(this.color);
                g2d.fillRoundRect(0, 0, this.minoSize, this.minoSize, borderRadiusX, borderRadiusY);
                g2d.setColor(this.uiConfig.getGridBorderColor());
                g2d.setStroke(new BasicStroke(this.uiConfig.getGridBorderThickness()));
                g2d.drawRoundRect(0, 0, this.minoSize, this.minoSize, borderRadiusX, borderRadiusY);
            }
        } else {
            g2d.setColor(this.uiConfig.getGridBackground());
            g2d.fillRoundRect(0, 0, this.minoSize, this.minoSize, borderRadiusX, borderRadiusY);
        }
    }

    public void setMinoSize(int minoSize) {
        this.minoSize = minoSize;
    }

}
