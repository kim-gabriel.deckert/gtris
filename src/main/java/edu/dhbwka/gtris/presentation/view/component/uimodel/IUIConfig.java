package edu.dhbwka.gtris.presentation.view.component.uimodel;

import edu.dhbwka.gtris.model.application.entities.primitives.MinoColor;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.util.Locale;
import javax.swing.border.Border;

public interface IUIConfig {
    MinoColor getMinoColor(char identifier);

    int getWindowMinWidth();

    int getWindowMinHeight();

    String getWindowTitle();

    String getMainMenuLogo();

    String getMusicThemefile();

    String getMusicSoundFont();

    int getMusicDefaultVolume();

    int getCountdown();

    int getMinimalMinosize();

    Color getCommonBackground();

    Font getButtonFont();

    Dimension getButtonSize();

    Color getButtonForeground();

    Color getButtonBackground();

    Font getCountdownFont();

    Color getCountdownForeground();

    int getMainMenuPadding();

    Font getMainMenuButtonFont();

    Color getMainMenuButtonForeground();

    Color getMainMenuButtonBackground();

    Border getMainMenuButtonBorder();

    int getMainMenuButtonWidth();

    int getMainMenuButtonHeight();

    Font getSettingsMenuHeadingFont();

    Color getSettingsMenuHeadingForeground();

    Font getSettingsMenuButtonFont();

    Color getSettingsMenuButtonForeground();

    Color getSettingsMenuButtonBackground();

    Border getSettingsMenuButtonBorder();

    Font getTableFont();

    Color getTableBackground();

    Color getTableForeground();

    Font getTableHeaderFont();

    Color getTableHeaderBackground();

    Color getTableHeaderForeground();

    Font getLabelFont();

    Color getLabelForeground();

    Color getLabelBackground();

    Font getTextfieldFont();

    Color getTextfieldForeground();

    Color getTextfieldBackground();

    Font getStatuswindowHeadingFont();

    Color getStatuswindowBackground();

    Color getStatuswindowForeground();

    Border getStatuswindowBorder();

    int getStatuswindowComponentPadding();

    Font getFragmentTitleFont();

    Color getFragmentTitleForeground();

    Border getFragmentBorder();

    int getFragmentPadding();

    Color getScorelabelBackground();

    Color getScorelabelForeground();

    Font getScorelabelFont();

    Color getGridBackground();

    int getGridBorderThickness();

    Color getGridBorderColor();

    int getGhostminoBorderThickness();

    Color getGhostminoBorderColor();

    float getMinoBorderRadiusXDivisor();

    float getMinoBorderRadiusYDivisor();

    float getGtrominoComponentPaddingXDivisor();

    float getGtrominoComponentPaddingYDivisor();

    int getViewUpdateTimer();

    int getTableRowHeight();

    Locale getDefaultLocale();
}
