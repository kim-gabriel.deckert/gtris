package edu.dhbwka.gtris.presentation.view.component.uimodel;

import java.awt.event.KeyEvent;
import java.util.EnumMap;
import java.util.HashMap;

public enum GameControls {
    ;
    public static final HashMap<Integer, GameAction> GameActionKeyCodes = new HashMap<>() {
        {
            put(Integer.valueOf(KeyEvent.VK_SPACE), GameAction.DROP_HARD);
            put(Integer.valueOf(KeyEvent.VK_NUMPAD8), GameAction.DROP_HARD);

            put(Integer.valueOf(KeyEvent.VK_DOWN), GameAction.DROP_SOFT);
            put(Integer.valueOf(KeyEvent.VK_NUMPAD2), GameAction.DROP_SOFT);

            put(Integer.valueOf(KeyEvent.VK_SHIFT), GameAction.HOLD);
            put(Integer.valueOf(KeyEvent.VK_C), GameAction.HOLD);
            put(Integer.valueOf(KeyEvent.VK_NUMPAD0), GameAction.HOLD);

            put(Integer.valueOf(KeyEvent.VK_ESCAPE), GameAction.PAUSE);
            put(Integer.valueOf(KeyEvent.VK_F1), GameAction.PAUSE);

            put(Integer.valueOf(KeyEvent.VK_UP), GameAction.ROTATE_CLOCKWISE);
            put(Integer.valueOf(KeyEvent.VK_X), GameAction.ROTATE_CLOCKWISE);
            put(Integer.valueOf(KeyEvent.VK_NUMPAD1), GameAction.ROTATE_CLOCKWISE);
            put(Integer.valueOf(KeyEvent.VK_NUMPAD5), GameAction.ROTATE_CLOCKWISE);
            put(Integer.valueOf(KeyEvent.VK_NUMPAD9), GameAction.ROTATE_CLOCKWISE);

            put(Integer.valueOf(KeyEvent.VK_CONTROL), GameAction.ROTATE_COUNTERCLOCKWISE);
            put(Integer.valueOf(KeyEvent.VK_Z), GameAction.ROTATE_COUNTERCLOCKWISE);
            put(Integer.valueOf(KeyEvent.VK_NUMPAD3), GameAction.ROTATE_COUNTERCLOCKWISE);
            put(Integer.valueOf(KeyEvent.VK_NUMPAD7), GameAction.ROTATE_COUNTERCLOCKWISE);

            put(Integer.valueOf(KeyEvent.VK_LEFT), GameAction.SHIFT_LEFT);
            put(Integer.valueOf(KeyEvent.VK_NUMPAD4), GameAction.SHIFT_LEFT);

            put(Integer.valueOf(KeyEvent.VK_RIGHT), GameAction.SHIFT_RIGHT);
            put(Integer.valueOf(KeyEvent.VK_NUMPAD6), GameAction.SHIFT_RIGHT);
        }

        @Override public HashMap<Integer, GameAction> clone()
                throws AssertionError {throw new AssertionError();}
    };
    private static final EnumMap<GameAction, Localizable> GameActionDescription =
            new GameActionLocalization();

    public static Localizable getGameActionLocalizable(GameAction action) {
        return GameActionDescription.get(action);
    }

    public static GameAction getGameAction(Integer keyCode) {
        return GameActionKeyCodes.get(keyCode);
    }

    public enum GameAction {
        PAUSE,
        SHIFT_LEFT,
        SHIFT_RIGHT,
        ROTATE_CLOCKWISE,
        ROTATE_COUNTERCLOCKWISE,
        DROP_HARD,
        DROP_SOFT,
        HOLD
    }

    private static class GameActionLocalization extends EnumMap<GameAction, Localizable> {
        {
            put(GameAction.DROP_HARD, Localizable.CONTROL_DROPHARD);
            put(GameAction.DROP_SOFT, Localizable.CONTROL_DROPSOFT);
            put(GameAction.HOLD, Localizable.CONTROL_HOLD);
            put(GameAction.ROTATE_CLOCKWISE, Localizable.CONTROL_ROTATECLOCKWISE);
            put(GameAction.ROTATE_COUNTERCLOCKWISE,
                    Localizable.CONTROL_ROTATECOUNTERCLOCKWISE);
            put(GameAction.SHIFT_LEFT, Localizable.CONTROL_SHIFTLEFT);
            put(GameAction.SHIFT_RIGHT, Localizable.CONTROL_SHIFTRIGHT);
            put(GameAction.PAUSE, Localizable.CONTROL_PAUSE);
        }

        GameActionLocalization() {super(GameAction.class);}

        @Override public final EnumMap<GameAction, Localizable> clone()
                throws AssertionError {throw new AssertionError();}
    }
}
