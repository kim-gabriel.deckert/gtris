package edu.dhbwka.gtris.presentation.view;

import edu.dhbwka.gtris.model.application.IHighScore;
import edu.dhbwka.gtris.model.application.IHighScoreRepository;
import edu.dhbwka.gtris.presentation.view.component.StyledPanel;
import edu.dhbwka.gtris.presentation.view.component.uimodel.Localizable;
import java.awt.BorderLayout;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class HighScoresPanel extends StyledPanel {
    private final String[] columnNames;
    private final DefaultTableModel scoreTableModel;
    private final IHighScoreRepository highScoreRepository;

    public HighScoresPanel(GameFrame gameFrame,
            IHighScoreRepository highScoreRepository) {
        super(gameFrame.getUiConfig());
        this.highScoreRepository = highScoreRepository;
        setLayout(new BorderLayout());
        columnNames = new String[]{ "#", gameFrame.localize(Localizable.LABEL_PLAYER),
                gameFrame.localize(Localizable.LABEL_DATE),
                gameFrame.localize(Localizable.LABEL_LEVEL),
                gameFrame.localize(Localizable.LABEL_ROWS),
                gameFrame.localize(Localizable.LABEL_SCORE) };
        scoreTableModel = new DefaultTableModel(columnNames, 0);
        JTable scoreTable = styleTable(new JTable(scoreTableModel));
        scoreTable.getColumnModel().getColumn(0).setMaxWidth(50);
        scoreTable.getColumnModel().getColumn(2).setMinWidth(200);
        JScrollPane scrollPane = new JScrollPane(scoreTable);
        add(scrollPane, BorderLayout.CENTER);
        JButton backButton =
                styleButton(new JButton(gameFrame.localize(Localizable.BUTTON_BACK)));
        backButton.addActionListener(e -> gameFrame.setContentPane(gameFrame.getMainMenuPanel()));
        add(backButton, BorderLayout.PAGE_END);
    }

    public void reload() {
        int numRows = scoreTableModel.getRowCount();
        for (int i = 0; i < numRows; i++) {
            scoreTableModel.removeRow(0);
        }
        for (String[] row : loadData()) {
            scoreTableModel.addRow(row);
        }
        scoreTableModel.fireTableDataChanged();
    }

    private String[][] loadData() {
        String[][] highScoreTableData;
        List<IHighScore> highScores = this.highScoreRepository.getHighScores();
        highScoreTableData = new String[highScores.size()][columnNames.length];
        int idx = 0;
        for (IHighScore highScore : highScores.stream().sorted().toList()) {
            String dateTime = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM)
                                            .format(highScore.getDateTime());
            highScoreTableData[idx] =
                    new String[]{ String.valueOf(idx + 1), highScore.getName(), dateTime,
                            String.valueOf(highScore.getLevel()),
                            String.valueOf(highScore.getRows()),
                            String.valueOf(highScore.getScore()) };
            idx++;
        }
        return highScoreTableData;
    }
}
