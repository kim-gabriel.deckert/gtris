package edu.dhbwka.gtris.presentation.view;

import edu.dhbwka.gtris.presentation.view.component.StyledPanel;
import edu.dhbwka.gtris.presentation.view.component.uimodel.Localizable;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JSlider;
import javax.swing.JToggleButton;
import javax.swing.event.ChangeEvent;

public class SettingsMenuPanel extends StyledPanel {
    private final GameFrame gameFrame;
    private JButton switchLanguageButton;
    private JButton backButton;
    private JToggleButton toggleMusicButton;
    private JSlider musicVolumeSlider;

    public SettingsMenuPanel(GameFrame gameFrame) {
        super(gameFrame.getUiConfig());
        this.gameFrame = gameFrame;
        setLayout(new GridLayout(3, 1));
        initComponents();
        addActionListener();
    }

    private void initComponents() {
        JLabel settingsMenuHeading = styleSettingsMenuHeading(
                new JLabel(gameFrame.localize(Localizable.LABEL_SETTINGS)));
        StyledPanel settingsMenuPanel = new StyledPanel(gameFrame.getUiConfig());
        JLabel languageLabel = styleLabel(new JLabel(getAvailableLanguages()));
        switchLanguageButton = (JButton) styleSettingsMenuButton(
                new JButton(gameFrame.localize(Localizable.BUTTON_SWITCHLANGUAGE)));
        toggleMusicButton = (JToggleButton) styleSettingsMenuButton(
                new JToggleButton(gameFrame.localize(Localizable.TOGGLE_MUSIC)));
        musicVolumeSlider = new JSlider(0, 127, gameFrame.getMusicVolume());
        backButton =
                styleButton(new JButton(gameFrame.localize(Localizable.BUTTON_BACK)));

        add(settingsMenuHeading);
        settingsMenuPanel.setLayout(new GridLayout(3, 2));
        settingsMenuPanel.add(languageLabel);
        settingsMenuPanel.add(switchLanguageButton);
        settingsMenuPanel.add(musicVolumeSlider);
        settingsMenuPanel.add(toggleMusicButton);
        settingsMenuPanel.add(backButton);
        add(settingsMenuPanel);
    }

    private void addActionListener() {
        switchLanguageButton.addActionListener(
                e -> gameFrame.switchLanguage(getClass()));
        toggleMusicButton.addActionListener(e -> gameFrame.toggleMusic());
        musicVolumeSlider.addChangeListener((ChangeEvent e) -> gameFrame.setMusicVolume(
                musicVolumeSlider.getValue()));
        backButton.addActionListener(
                e -> gameFrame.setContentPane(gameFrame.getMainMenuPanel()));
    }

    private String getAvailableLanguages() {
        return String.join(", ", gameFrame.getAvailableLanguages());
    }

}
