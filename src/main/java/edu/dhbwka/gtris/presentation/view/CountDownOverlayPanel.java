package edu.dhbwka.gtris.presentation.view;

import edu.dhbwka.gtris.presentation.view.component.uimodel.IUIConfig;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.Timer;

class CountDownOverlayPanel extends OverlayPanel {

    private final JLabel label = this.styleLabel(new JLabel());
    private final GameFrame gameFrame;
    private final Timer timer = new Timer(0, new CountDownListener());
    private int value;
    private Runnable runnable;

    CountDownOverlayPanel(GameFrame gameFrame) {
        super(Type.COUNTDOWN, gameFrame);
        this.gameFrame = gameFrame;
        this.init();
    }

    private void init() {
        this.setLayout(new BorderLayout());
        this.timer.setRepeats(true);
        this.timer.setDelay(1_000);
        this.label.setForeground(this.gameFrame.getUiConfig().getCountdownForeground());
        this.label.setText(String.valueOf(this.gameFrame.getUiConfig().getCountdown()));
        this.label.setHorizontalAlignment(SwingConstants.CENTER);
        this.label.setVerticalAlignment(SwingConstants.CENTER);
        this.label.setFont(this.gameFrame.getUiConfig().getCountdownFont());
        this.add(this.label, BorderLayout.CENTER);
    }

    public void start(Runnable runnable) {
        this.runnable = runnable;
        IUIConfig uiConfig = this.gameFrame.getUiConfig();
        this.value = uiConfig.getCountdown();
        this.timer.restart();
    }

    public void stop() {
        this.runnable = null;
        this.timer.stop();
    }

    private class CountDownListener implements ActionListener {
        @Override public void actionPerformed(final ActionEvent actionEvent) {
            CountDownOverlayPanel.this.label.setText(String.valueOf(CountDownOverlayPanel.this.value));
            if (0 < value) {
                CountDownOverlayPanel.this.value--;
            } else {
                if (null != runnable) {
                    CountDownOverlayPanel.this.runnable.run();
                    CountDownOverlayPanel.this.runnable = null;
                }
                CountDownOverlayPanel.this.timer.stop();
            }
        }
    }
}
