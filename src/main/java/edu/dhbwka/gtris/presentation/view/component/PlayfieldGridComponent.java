package edu.dhbwka.gtris.presentation.view.component;

import edu.dhbwka.gtris.model.application.entities.IMino;
import edu.dhbwka.gtris.model.application.entities.primitives.Position;
import edu.dhbwka.gtris.presentation.view.component.uimodel.IUIConfig;
import java.awt.BasicStroke;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.LayoutManager;
import java.util.ArrayList;
import java.util.Collection;
import javax.swing.JComponent;

public class PlayfieldGridComponent extends JComponent {
    private final int columns;
    private final int rows;
    private final MinoComponent[][] minoComponents;
    private final int[][] minoViewStates;
    private final IUIConfig uiConfig;

    public PlayfieldGridComponent(final int columns, final int rows, final IUIConfig uiConfig) {
        this.columns = columns;
        this.rows = rows;
        this.uiConfig = uiConfig;
        this.minoComponents = new MinoComponent[columns][rows];
        this.minoViewStates = new int[columns][rows];
        final LayoutManager minoMatrixLayout =
                new MinoMatrixLayout(columns, rows, uiConfig.getMinimalMinosize(), this);
        this.setLayout(minoMatrixLayout);
        for (int y = 0; y < rows; y++) {
            for (int x = 0; x < columns; x++) {
                this.minoComponents[x][y] = MinoComponent.createMinoComponent(uiConfig, this.getMinoSize(),
                        new Position(x, y));
                if (null != this.minoComponents[x][y]) {
                    this.minoViewStates[x][y] = this.minoComponents[x][y].getState();
                }
                this.add(this.minoComponents[x][y]);
            }
        }
        this.setOpaque(false);
        this.setFocusable(false);
    }

    public int getMinoSize() {
        final int minoWidth = this.getSize().width / this.columns;
        final int minoHeight = this.getSize().height / this.rows;
        final int csize = Math.min(minoWidth, minoHeight);
        return Math.max(csize, this.uiConfig.getMinimalMinosize());
    }

    public void renderMinos(final Collection<? extends IMino> regularMinos,
            final Collection<? extends IMino> ghostMinos) {
        this.resize();

        final Collection<Position> updateCells = new ArrayList<>(
                regularMinos.stream().map(IMino::getPosition)
                        .filter(position -> isVisiblePosition(position)).toList());
        updateCells.addAll(ghostMinos.stream().map(IMino::getPosition)
                                   .filter(position -> isVisiblePosition(position)).toList());
        final int minoSize = this.getMinoSize();
        final Graphics g = this.getGraphics();
        for (int x = 0; x < this.columns; x++) {
            for (int y = 0; y < this.rows; y++) {
                this.minoComponents[x][y].setMinoSize(minoSize);
                ghostMinos.stream().filter(mino -> isVisiblePosition(mino.getPosition()))
                        .forEach(mino -> {
                            final int[] rgba = mino.getColor().getRgba();
                            this.minoComponents[mino.getPosition().getX()][mino.getPosition()
                                                                              .getY()].setAttributes(
                                    true, rgba[0], rgba[1], rgba[2], rgba[3]);
                        });
                regularMinos.stream().filter(mino -> isVisiblePosition(mino.getPosition()))
                        .forEach(mino -> {
                            final int[] rgba = mino.getColor().getRgba();
                            this.minoComponents[mino.getPosition().getX()][mino.getPosition()
                                                                              .getY()].setAttributes(
                                    false, rgba[0], rgba[1], rgba[2], rgba[3]);
                        });
                if (!updateCells.contains(new Position(x, y))) {
                    this.minoComponents[x][y].reset();
                }
                if (this.minoViewStates[x][y] != this.minoComponents[x][y].getState()) {
                    this.minoComponents[x][y].repaint();
                    this.minoViewStates[x][y] = this.minoComponents[x][y].getState();
                }
                this.renderMino(g, x, y);
            }
        }
        this.paintComponent(g);
    }

    public void resize() {
        final int minoSize = this.getMinoSize();
        for (int x = 0; x < this.columns; x++) {
            for (int y = 0; y < this.rows; y++) {
                this.minoComponents[x][y].setMinoSize(minoSize);
            }
        }
    }

    private boolean isVisiblePosition(Position position) {
        return 0 <= position.getX() && position.getX() < this.columns && 0 <= position.getY() &&
                       position.getY() < this.rows;
    }

    private void renderMino(final Graphics g, final int x, final int y) {
        final int minoSize = this.getMinoSize();
        final int borderRadiusX = (int) (minoSize / this.uiConfig.getMinoBorderRadiusXDivisor());
        final int borderRadiusY = (int) (minoSize / this.uiConfig.getMinoBorderRadiusYDivisor());
        if (null != g) {
            final Graphics2D g2d = (Graphics2D) g;
            g2d.setColor(this.uiConfig.getGridBorderColor());
            g2d.setStroke(new BasicStroke(this.uiConfig.getGridBorderThickness()));
            g2d.drawRoundRect(x * minoSize, y * minoSize, minoSize, minoSize, borderRadiusX,
                    borderRadiusY);
        }
    }

    @Override protected void paintComponent(final Graphics g) {
        super.paintComponent(g);
        if (null != g) {
            final Graphics2D g2d = (Graphics2D) g;
            g2d.setColor(this.uiConfig.getGridBorderColor());
            g2d.setStroke(new BasicStroke((this.uiConfig.getGridBorderThickness() * 4)));
            g.drawRect(0, 0, this.columns * this.getMinoSize(), this.rows * this.getMinoSize());
        }
    }
}
