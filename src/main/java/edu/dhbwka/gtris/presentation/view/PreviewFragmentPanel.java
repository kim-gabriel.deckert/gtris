package edu.dhbwka.gtris.presentation.view;

import edu.dhbwka.gtris.model.application.entities.GTromino;
import edu.dhbwka.gtris.presentation.view.component.GtrominoComponent;
import edu.dhbwka.gtris.presentation.view.component.StyledPanel;
import edu.dhbwka.gtris.presentation.view.component.uimodel.IUIConfig;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.util.stream.IntStream;

class PreviewFragmentPanel extends FragmentPanel {

    private final GtrominoComponent[] gtrominoComponents;
    private final GTromino[] gtrominoTypes;

    PreviewFragmentPanel(final IUIConfig uiConfig, final String heading, final int previewCount) {
        super(uiConfig, heading);
        StyledPanel previewPanel = new StyledPanel(uiConfig);
        previewPanel.setLayout(new GridLayout(previewCount, 1));
        this.gtrominoComponents = new GtrominoComponent[previewCount];
        this.gtrominoTypes = new GTromino[previewCount];
        for (int i = 0; i < previewCount; i++) {
            this.gtrominoComponents[i] = new GtrominoComponent(uiConfig);
            previewPanel.add(this.gtrominoComponents[i]);
        }
        this.add(previewPanel, BorderLayout.CENTER);
        this.addComponentListener(new ResizeComponentAdapter());
        this.resize();
    }

    public void resize() {
        for (final GtrominoComponent gtrominoComponent : this.gtrominoComponents) {
            gtrominoComponent.resize();
        }
    }

    public void setGtrominoTypes(final GTromino[] gtrominoTypes) {
        IntStream.range(0, gtrominoTypes.length).forEach(i -> {
            this.gtrominoTypes[i] = gtrominoTypes[i];
            this.gtrominoComponents[i].setGtromino(this.gtrominoTypes[i]);
        });
    }

    private class ResizeComponentAdapter extends ComponentAdapter {
        @Override public void componentResized(final ComponentEvent e) {
            PreviewFragmentPanel.this.resize();
        }
    }
}
