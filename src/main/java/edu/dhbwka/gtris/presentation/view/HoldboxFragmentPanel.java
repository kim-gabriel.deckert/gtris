package edu.dhbwka.gtris.presentation.view;

import edu.dhbwka.gtris.model.application.entities.GTromino;
import edu.dhbwka.gtris.presentation.view.component.GtrominoComponent;
import edu.dhbwka.gtris.presentation.view.component.uimodel.IUIConfig;
import java.awt.BorderLayout;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

class HoldboxFragmentPanel extends FragmentPanel {

    private final GtrominoComponent gtrominoComponent;

    HoldboxFragmentPanel(IUIConfig uiConfig, String heading) {
        super(uiConfig, heading);
        gtrominoComponent = new GtrominoComponent(uiConfig);
        add(gtrominoComponent, BorderLayout.CENTER);
        this.addComponentListener(new ResizeComponentAdapter());
        resize();
    }

    public void resize() {
        gtrominoComponent.resize();
    }

    public void setGtromino(GTromino gtromino) {
        if (null != gtromino) {
            gtrominoComponent.setGtromino(gtromino);
        }
    }

    private class ResizeComponentAdapter extends ComponentAdapter {
        @Override public void componentResized(ComponentEvent e) {
            resize();
        }
    }
}
