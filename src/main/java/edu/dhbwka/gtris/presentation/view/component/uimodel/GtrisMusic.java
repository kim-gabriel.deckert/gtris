package edu.dhbwka.gtris.presentation.view.component.uimodel;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Objects;
import java.util.stream.Stream;
import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiChannel;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Sequencer;
import javax.sound.midi.Soundbank;
import javax.sound.midi.Synthesizer;
import javax.sound.midi.Transmitter;

public final class GtrisMusic implements Runnable {
    private static GtrisMusic instance;
    private final String themeResource;
    private final Sequencer sequencer;
    private final Synthesizer synthesizer;
    private final Soundbank soundbank;
    private Thread themeThread;
    private int volume;

    private GtrisMusic(final String themeResource, final String soundFontResource, final int volume) {
        this.themeResource = themeResource;
        this.volume = volume;
        ClassLoader classLoader = this.getClass().getClassLoader();
        InputStream soundFontInputStream =
                Objects.requireNonNull(classLoader.getResourceAsStream(soundFontResource));
        try {
            this.sequencer = MidiSystem.getSequencer(false);
            this.synthesizer = MidiSystem.getSynthesizer();
            this.soundbank = MidiSystem.getSoundbank(soundFontInputStream);
        } catch (final MidiUnavailableException | InvalidMidiDataException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static GtrisMusic getInstance(final String themeResource, final String soundFontResource,
            final int volume) {
        if (null == GtrisMusic.instance) {
            instance = new GtrisMusic(themeResource, soundFontResource, volume);
        }
        return instance;
    }

    public int getVolume() {
        return this.volume;
    }

    public void setVolume(final int volume) {
        if (0 <= volume && 128 > volume) {
            this.volume = volume;
        } else {
            throw new IllegalArgumentException();
        }
        Stream<MidiChannel> stream = Arrays.stream(this.synthesizer.getChannels());
        stream.forEach(channel -> channel.controlChange(7, volume));
    }

    public void start() {
        if (null == themeThread) {
            this.themeThread = new Thread(this);
        }
        this.themeThread.start();
        this.sequencer.addMetaEventListener(meta -> {
            final int type = meta.getType();
            if (89 == type) {
                Arrays.stream(this.synthesizer.getChannels())
                        .filter(channel -> channel.getController(7) != this.volume)
                        .forEach(channel -> channel.controlChange(7, this.volume));
                Arrays.stream(this.synthesizer.getChannels()).filter(MidiChannel::getMute)
                        .forEach(channel -> channel.setMute(false));
            }
        });
    }

    public void stop() {
        if (null != sequencer && this.sequencer.isOpen()) {
            this.sequencer.stop();
            this.sequencer.close();
        }
        if (null != synthesizer && this.synthesizer.isOpen()) {
            this.synthesizer.close();
        }
        this.themeThread = null;
    }

    public boolean isMusicRunning() {
        return null != themeThread;
    }

    @Override public void run() {
        ClassLoader classLoader = this.getClass().getClassLoader();
        InputStream themeFileInputStream =
                Objects.requireNonNull(classLoader.getResourceAsStream(this.themeResource));
        try {
            this.sequencer.open();
            this.synthesizer.open();
            this.synthesizer.unloadAllInstruments(this.synthesizer.getDefaultSoundbank());
            this.synthesizer.loadAllInstruments(this.soundbank);
            Transmitter transmitter = this.sequencer.getTransmitter();
            transmitter.setReceiver(this.synthesizer.getReceiver());
            this.sequencer.setSequence(MidiSystem.getSequence(themeFileInputStream));
            this.sequencer.setLoopCount(Sequencer.LOOP_CONTINUOUSLY);
            this.sequencer.start();
            Stream<MidiChannel> stream = Arrays.stream(this.synthesizer.getChannels());
            stream.forEach(GtrisMusic::accept);
        } catch (final MidiUnavailableException | InvalidMidiDataException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static void accept(final MidiChannel c) {c.setMute(true);}
}
