package edu.dhbwka.gtris.presentation.view.component;

import edu.dhbwka.gtris.model.application.entities.primitives.Position;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.LayoutManager2;
import java.awt.Point;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JComponent;

class MinoMatrixLayout implements LayoutManager2 {
    private final JComponent container;
    private final int columns;
    private final int rows;
    private final int minimalMinoSize;
    private final Map<Position, MinoComponent> components = new HashMap<>();

    public MinoMatrixLayout(int columns, int rows, int minimalMinoSize,
            JComponent container) {
        this.columns = columns;
        this.rows = rows;
        this.minimalMinoSize = minimalMinoSize;
        this.container = container;
    }

    @Override public void addLayoutComponent(String name, Component comp) {
        this.addLayoutComponent(comp);
    }

    private void addLayoutComponent(final Component comp) {
        if (comp instanceof MinoComponent && null != ((MinoComponent) comp).getPosition()) {
            this.components.put(((MinoComponent) comp).getPosition(), (MinoComponent) comp);
        } else {
            throw new IllegalArgumentException();
        }
    }

    @Override public void removeLayoutComponent(Component comp) {
        this.components.remove(((MinoComponent) comp).getPosition());
    }

    @Override public Dimension preferredLayoutSize(Container parent) {
        Dimension result;
        if (parent instanceof StyledPanel) {
            int minosize = this.getMinoSize(parent.getSize().width, parent.getSize().height);
            result = new Dimension(minosize * this.columns, minosize * this.rows);
        } else {
            int parentWidth = parent.getSize().width;
            int parentHeight = parent.getSize().height;
            int usedParentWidthTop = Arrays.stream(parent.getComponents())
                                                   .filter(component -> 0 == component.getLocation()
                                                                                     .getY())
                                                   .mapToInt(component -> component.getSize().width)
                                                   .sum();
            int totalWidth = Arrays.stream(parent.getComponents())
                                           .filter(component -> component.getLocation().getY() ==
                                                                        (parentHeight - 1))
                                           .mapToInt(component -> component.getSize().width).sum();
            int usedParentWidth = Math.min(usedParentWidthTop, totalWidth);
            result = this.calculateDimension(parentWidth, parentHeight, usedParentWidth);
        }
        return result;
    }

    @Override public Dimension minimumLayoutSize(Container parent) {
        return new Dimension(this.minimalMinoSize * this.columns, this.minimalMinoSize * this.rows);
    }

    @Override public void layoutContainer(Container parent) {
        int parentWidth = parent.getSize().width;
        int parentHeight = parent.getSize().height;
        int minoSize = this.getMinoSize(parentWidth, parentHeight);
        int offsetX = (int) Math.floor(((parentWidth - (float) this.columns * minoSize) / 2.0f));
        int offsetY = (int) Math.floor(((parentHeight - (float) this.rows * minoSize) / 2.0f));
        this.container.setLocation(offsetX, offsetY);
        for (Map.Entry<Position, MinoComponent> entry : this.components.entrySet()) {
            Position p = entry.getKey();
            Component component = entry.getValue();
            component.setLocation(new Point(p.getX() * minoSize, p.getY() * minoSize));
            component.setSize(minoSize, minoSize);
        }

    }

    private int getMinoSize(int targetWidth, int targetHeight) {
        int minoWidth = targetWidth / this.columns;
        int minoHeight = targetHeight / this.rows;
        return Math.min(minoWidth, minoHeight);
    }

    private Dimension calculateDimension(int parentWidth, int parentHeight,
            int usedParentWidth) {
        float rPc = (float) this.rows / this.columns;
        float cPr = (float) this.columns / this.rows;
        int availableParentWidth = parentWidth - usedParentWidth;
        float parentRatio = (float) availableParentWidth / parentHeight;
        int targetHeight;
        int targetWidth;
        if (parentRatio < cPr) {
            targetHeight = this.getTargetHeight(availableParentWidth);
            targetWidth = (int) Math.floor((targetHeight * cPr));
        } else {
            targetWidth = this.getTargetWidth(parentHeight);
            targetHeight = (int) Math.floor((targetWidth * rPc));
        }
        return new Dimension(Math.max(targetWidth, 0), Math.max(targetHeight, 0));
    }

    private int getTargetHeight(int containerWidth) {
        float ratio = (float) this.rows / this.columns;
        return (int) Math.floor((containerWidth * ratio));
    }

    private int getTargetWidth(int containerHeight) {
        float ratio = (float) this.rows / this.columns;
        return (int) Math.floor((containerHeight / ratio));
    }

    @Override public void addLayoutComponent(Component component, Object constraints) {
        this.addLayoutComponent(component);
    }

    @Override public Dimension maximumLayoutSize(Container target) {
        Dimension result;
        if (target instanceof PlayfieldGridComponent) {
            result = new Dimension(((PlayfieldGridComponent) target).getMinoSize() * this.columns,
                    ((PlayfieldGridComponent) target).getMinoSize() * this.rows);

        } else {
            int parentWidth = target.getParent().getSize().width;
            int parentHeight = target.getParent().getSize().height;
            int usedParentWidthTop = Arrays.stream(target.getParent().getComponents())
                                                   .filter(component -> !component.equals(target) &&
                                                                                0 ==
                                                                                        component.getLocation()
                                                                                                .getY())
                                                   .mapToInt(component -> component.getSize().width)
                                                   .sum();
            int accumulatedWidth = 0;
            for (Component component : target.getParent().getComponents()) {
                if (!component.equals(target) &&
                            component.getLocation().getY() == (parentHeight - 1)) {
                    int width = component.getSize().width;
                    accumulatedWidth += width;
                }
            }
            int usedParentWidth = Math.min(usedParentWidthTop, accumulatedWidth);
            result = this.calculateDimension(parentWidth, parentHeight, usedParentWidth);
        }

        return result;
    }

    @Override public float getLayoutAlignmentX(Container target) {
        return 0.0f;
    }

    @Override public float getLayoutAlignmentY(Container target) {
        return 0.0f;
    }

    @Override public void invalidateLayout(Container target) {
    }
}
