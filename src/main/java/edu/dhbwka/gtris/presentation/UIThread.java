package edu.dhbwka.gtris.presentation;

import edu.dhbwka.gtris.model.application.IGameConfiguration;
import edu.dhbwka.gtris.presentation.view.GameUI;
import edu.dhbwka.gtris.presentation.view.component.uimodel.IUIConfig;

public class UIThread implements Runnable {

    private final IUIConfig uiConfig;
    private final IGameConfiguration gameConfiguration;

    public UIThread(final IUIConfig uiConfig, final IGameConfiguration gameConfiguration) {
        this.uiConfig = uiConfig;
        this.gameConfiguration = gameConfiguration;
    }

    @Override public void run() {
        GameUI gameUI = new GameUI(this.uiConfig, this.gameConfiguration);
    }
}
