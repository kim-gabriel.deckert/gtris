package edu.dhbwka.gtris;

import edu.dhbwka.gtris.customization.DefaultGameConfiguration;
import edu.dhbwka.gtris.customization.DefaultUIConfig;
import edu.dhbwka.gtris.model.application.IGameConfiguration;
import edu.dhbwka.gtris.presentation.UIThread;
import edu.dhbwka.gtris.presentation.view.component.uimodel.IUIConfig;
import javax.swing.SwingUtilities;

final class Main {
    private Main() {
    }

    public static void main(final String[] args) {
        final IGameConfiguration gameConfiguration = new DefaultGameConfiguration();
        final IUIConfig uiConfig = new DefaultUIConfig();

        final Runnable uiThread = new UIThread(uiConfig, gameConfiguration);
        Main.start(uiThread);
    }

    private static void start(final Runnable uiThread) {
        SwingUtilities.invokeLater(uiThread);
    }
}
