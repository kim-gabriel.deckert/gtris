package edu.dhbwka.gtris.customization;

import edu.dhbwka.gtris.model.application.IGameConfiguration;
import edu.dhbwka.gtris.model.application.IGameStateRepository;
import edu.dhbwka.gtris.model.application.IHighScoreRepository;
import edu.dhbwka.gtris.model.application.IScoreEvaluator;
import java.io.File;

public class DefaultGameConfiguration implements IGameConfiguration {

    public DefaultGameConfiguration() {}

    @Override public int getPlayfieldWidth() {
        return 10;
    }

    @Override public int getPlayfieldHeight() {
        return 20;
    }

    @Override public int getPreviewCount() {
        return 4;
    }

    @Override public int getDropInterval(final int level) {
        final float dropInterval = (float) (800.0 * StrictMath.pow(0.8 - ((float) (level - 1) * 0.007),
                (float) (level - 1)));
        return (int) Math.max(dropInterval, 50.0f);
    }

    @Override public IScoreEvaluator getScoreEvaluator() {
        return new DefaultScoreEvaluator();
    }

    @Override public IHighScoreRepository getHighScoreRepository() {
        final String highScoreFilePath = this.getRepositoryBaseDir() + File.separator + "HighScores.bin";
        final int keepNumberHighscores = 20;
        return new DefaultHighScoreFileRepository(highScoreFilePath, keepNumberHighscores);
    }

    private String getRepositoryBaseDir() {
        return System.getProperty("user.home") + File.separator + "GTris";
    }

    @Override public IGameStateRepository getGameStateRepository() {
        final String gameStateFile = this.getRepositoryBaseDir() + File.separator + "GameState.bin";
        return new DefaultGameStateFileRepository(gameStateFile);
    }

    @Override public void registerGTrominos() {
    }
}
