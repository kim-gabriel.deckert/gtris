package edu.dhbwka.gtris.customization;

import com.formdev.flatlaf.fonts.roboto.FlatRobotoFont;
import com.formdev.flatlaf.ui.FlatLineBorder;
import edu.dhbwka.gtris.model.application.entities.primitives.MinoColor;
import edu.dhbwka.gtris.presentation.view.component.uimodel.IUIConfig;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import javax.swing.border.Border;

public class DefaultUIConfig implements IUIConfig {
    private static final Map<Character, MinoColor> minoColors = new HashMap<>() {{
        put('I', new MinoColor(0, 192, 192, 255));
        put('J', new MinoColor(0, 0, 192, 255));
        put('L', new MinoColor(192, 128, 0, 255));
        put('O', new MinoColor(192, 192, 0, 255));
        put('S', new MinoColor(0, 192, 0, 255));
        put('T', new MinoColor(128, 0, 192, 255));
        put('Z', new MinoColor(192, 0, 0, 255));
    }};
    private final Font buttonFont = new Font(FlatRobotoFont.FAMILY_LIGHT, Font.PLAIN, 24);
    private final Dimension buttonSize = new Dimension(160, 80);
    private final Font countdownFont = new Font(FlatRobotoFont.FAMILY_SEMIBOLD, Font.BOLD, 120);
    private final Font mainMenuButtonFont =
            new Font(FlatRobotoFont.FAMILY_SEMIBOLD, Font.PLAIN, 32);
    private final Font settingsMenuButtonFont =
            new Font(FlatRobotoFont.FAMILY_SEMIBOLD, Font.PLAIN, 32);
    private final Font settingsMenuHeadingFont =
            new Font(FlatRobotoFont.FAMILY_SEMIBOLD, Font.BOLD, 32);
    private final Font tableFont = new Font(FlatRobotoFont.FAMILY_SEMIBOLD, Font.PLAIN, 20);
    private final Font tableHeaderFont = new Font(FlatRobotoFont.FAMILY_SEMIBOLD, Font.PLAIN, 24);
    private final Font labelFont = new Font(FlatRobotoFont.FAMILY, Font.PLAIN, 18);
    private final Font textfieldFont = new Font(FlatRobotoFont.FAMILY_SEMIBOLD, Font.PLAIN, 24);
    private final Font statuswindowHeadingFont =
            new Font(FlatRobotoFont.FAMILY_SEMIBOLD, Font.BOLD, 32);
    private final Font fragmentTitleFont = new Font(FlatRobotoFont.FAMILY_SEMIBOLD, Font.BOLD, 20);
    private final Font scorelabelFont = new Font(FlatRobotoFont.FAMILY, Font.PLAIN, 24);
    private final Locale defaultLocale = Locale.ENGLISH;
    private final Color colorI = Color.decode("#FFFFFF");
    private final Color commonForeground = this.colorI;
    private final Color statuswindowForeground = this.commonForeground;
    private final Border statuswindowBorder =
            new FlatLineBorder(new Insets(10, 30, 30, 30), this.statuswindowForeground, 3.0F, 15);
    private final Color fragmentTitleForeground = this.commonForeground;
    private final Border fragmentBorder =
            new FlatLineBorder(new Insets(5, 5, 5, 5), this.commonForeground, 2.0F, 10);
    private final Color countdownForeground = this.colorI;
    private final Color settingsMenuHeadingForeground = this.colorI;
    private final Color tableHeaderForeground = this.colorI;
    private final Color labelForeground = this.colorI;
    private final Color scorelabelForeground = this.colorI;
    private final Color colorII = Color.decode("#E1E5F2");
    private final Color tableBackground = this.colorII;
    private final Color scorelabelBackground = this.colorII;
    private final Color gridBorderColor = this.colorII;
    private final Color ghostminoBorderColor = this.colorII;
    private final Color colorIII = Color.decode("#BFDBF7");
    private final Color buttonBackground = this.colorIII;
    private final Color mainMenuButtonBackground = this.buttonBackground;
    private final Border mainMenuButtonBorder =
            new FlatLineBorder(new Insets(10, 10, 10, 10), this.mainMenuButtonBackground, 5.0F, 15);
    private final Color settingsMenuButtonBackground = this.buttonBackground;
    private final Border settingsMenuButtonBorder =
            new FlatLineBorder(new Insets(10, 10, 10, 10), this.settingsMenuButtonBackground, 5.0F, 15);
    private final Color textfieldBackground = this.colorIII;
    private final Color colorIV = Color.decode("#1F7A8C");
    private final Color settingsMenuHeadingBackground = this.colorIV;
    private final Color tableHeaderBackground = this.colorIV;
    private final Color colorV = Color.decode("#022B3A");
    private final Color commonBackground = this.colorV;
    private final Color statuswindowBackground = this.commonBackground;
    private final Color labelBackground = this.colorV;
    private final Color gridBackground = this.colorV;
    private final Color colorVI = Color.decode("#000000");
    private final Color buttonForeground = this.colorVI;
    private final Color mainMenuButtonForeground = this.buttonForeground;
    private final Color settingsMenuButtonForeground = this.buttonForeground;
    private final Color tableForeground = this.colorVI;
    private final Color textfieldForeground = this.colorVI;

    public DefaultUIConfig() {
    }

    public MinoColor getMinoColor(char identifier) {
        return DefaultUIConfig.minoColors.getOrDefault(identifier, null);
    }

    @Override public int getWindowMinWidth() {
        return 640;
    }

    @Override public int getWindowMinHeight() {
        return 640;
    }

    @Override public String getWindowTitle() {
        return "GTris";
    }

    @Override public String getMainMenuLogo() {
        return "gtris.svg";
    }

    @Override public String getMusicThemefile() {
        return "gtris.mid";
    }

    @Override public String getMusicSoundFont() {
        return "gtris.sf2";
    }

    @Override public int getMusicDefaultVolume() {
        return 30;
    }

    @Override public int getCountdown() {
        return 2;
    }

    @Override public int getMinimalMinosize() {
        return 10;
    }

    @Override public Color getCommonBackground() {
        return this.commonBackground;
    }

    @Override public Font getButtonFont() {
        return this.buttonFont;
    }

    @Override public Dimension getButtonSize() {
        return this.buttonSize;
    }

    @Override public Color getButtonForeground() {
        return this.buttonForeground;
    }

    @Override public Color getButtonBackground() {
        return this.buttonBackground;
    }

    @Override public Font getCountdownFont() {
        return this.countdownFont;
    }

    @Override public Color getCountdownForeground() {
        return this.countdownForeground;
    }

    @Override public int getMainMenuPadding() {
        return 20;
    }

    @Override public Font getMainMenuButtonFont() {
        return this.mainMenuButtonFont;
    }

    @Override public Color getMainMenuButtonForeground() {
        return this.mainMenuButtonForeground;
    }

    @Override public Color getMainMenuButtonBackground() {
        return this.mainMenuButtonBackground;
    }

    @Override public Border getMainMenuButtonBorder() {
        return this.mainMenuButtonBorder;
    }

    @Override public int getMainMenuButtonWidth() {
        return 200;
    }

    @Override public int getMainMenuButtonHeight() {
        return 80;
    }

    @Override public Font getSettingsMenuHeadingFont() {
        return this.settingsMenuHeadingFont;
    }

    @Override public Color getSettingsMenuHeadingForeground() {
        return this.settingsMenuHeadingForeground;
    }

    @Override public Font getSettingsMenuButtonFont() {
        return this.settingsMenuButtonFont;
    }

    @Override public Color getSettingsMenuButtonForeground() {
        return this.settingsMenuButtonForeground;
    }

    @Override public Color getSettingsMenuButtonBackground() {
        return this.settingsMenuButtonBackground;
    }

    @Override public Border getSettingsMenuButtonBorder() {
        return this.settingsMenuButtonBorder;
    }

    @Override public Font getTableFont() {
        return this.tableFont;
    }

    @Override public Color getTableBackground() {
        return this.tableBackground;
    }

    @Override public Color getTableForeground() {
        return this.tableForeground;
    }

    @Override public Font getTableHeaderFont() {
        return this.tableHeaderFont;
    }

    @Override public Color getTableHeaderBackground() {
        return this.tableHeaderBackground;
    }

    @Override public Color getTableHeaderForeground() {
        return this.tableHeaderForeground;
    }

    @Override public Font getLabelFont() {
        return this.labelFont;
    }

    @Override public Color getLabelForeground() {
        return this.labelForeground;
    }

    @Override public Color getLabelBackground() {
        return this.labelBackground;
    }

    @Override public Font getTextfieldFont() {
        return this.textfieldFont;
    }

    @Override public Color getTextfieldForeground() {
        return this.textfieldForeground;
    }

    @Override public Color getTextfieldBackground() {
        return this.textfieldBackground;
    }

    @Override public Font getStatuswindowHeadingFont() {
        return this.statuswindowHeadingFont;
    }

    @Override public Color getStatuswindowBackground() {
        return this.statuswindowBackground;
    }

    @Override public Color getStatuswindowForeground() {
        return this.statuswindowForeground;
    }

    @Override public Border getStatuswindowBorder() {
        return this.statuswindowBorder;
    }

    @Override public int getStatuswindowComponentPadding() {
        return 10;
    }

    @Override public Font getFragmentTitleFont() {
        return this.fragmentTitleFont;
    }

    @Override public Color getFragmentTitleForeground() {
        return this.fragmentTitleForeground;
    }

    @Override public Border getFragmentBorder() {
        return this.fragmentBorder;
    }

    @Override public int getFragmentPadding() {
        return 10;
    }

    @Override public Color getScorelabelBackground() {
        return this.scorelabelBackground;
    }

    @Override public Color getScorelabelForeground() {
        return this.scorelabelForeground;
    }

    @Override public Font getScorelabelFont() {
        return this.scorelabelFont;
    }

    @Override public Color getGridBackground() {
        return this.gridBackground;
    }

    @Override public int getGridBorderThickness() {
        return 1;
    }

    @Override public Color getGridBorderColor() {
        return this.gridBorderColor;
    }

    @Override public int getGhostminoBorderThickness() {
        return 3;
    }

    @Override public Color getGhostminoBorderColor() {
        return this.ghostminoBorderColor;
    }

    @Override public float getMinoBorderRadiusXDivisor() {
        return 6.0f;
    }

    @Override public float getMinoBorderRadiusYDivisor() {
        return 6.0f;
    }

    @Override public float getGtrominoComponentPaddingXDivisor() {
        return 2.0f;
    }

    @Override public float getGtrominoComponentPaddingYDivisor() {
        return 2.0f;
    }

    @Override public int getViewUpdateTimer() {
        return 10;
    }

    @Override public int getTableRowHeight() {
        return 60;
    }

    @Override public Locale getDefaultLocale() {
        return this.defaultLocale;
    }
}
