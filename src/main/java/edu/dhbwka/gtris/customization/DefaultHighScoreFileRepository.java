package edu.dhbwka.gtris.customization;

import edu.dhbwka.gtris.model.application.AbstractFileRepository;
import edu.dhbwka.gtris.model.application.HighScore;
import edu.dhbwka.gtris.model.application.IHighScore;
import edu.dhbwka.gtris.model.application.IHighScoreRepository;
import edu.dhbwka.gtris.model.application.entities.IReadableScore;
import java.io.IOException;
import java.util.ArrayList;
import java.util.stream.Collectors;

public class DefaultHighScoreFileRepository extends AbstractFileRepository<ArrayList<IHighScore>>
        implements IHighScoreRepository {
    private final int numberKeepHighScores;
    private ArrayList<IHighScore> highScores;

    public DefaultHighScoreFileRepository(final String filePathString, int numberKeepHighScores) {
        super(filePathString);
        this.numberKeepHighScores = numberKeepHighScores;
        highScores = this.getHighScores();
    }

    @Override public ArrayList<IHighScore> getHighScores() {
        try {
            this.highScores = this.loadEntitiesFromFile();
        } catch (final ClassNotFoundException | IOException ignored) {
        }

        return null != highScores ? this.highScores : new ArrayList<>();
    }

    @Override public void addHighScore(final IReadableScore score, final String playerName) {
        highScores.add(new HighScore(score, playerName));
        this.saveHighScores();
    }

    private void saveHighScores() {
        if (highScores.size() > this.numberKeepHighScores) {
            highScores = new ArrayList<>(
                    highScores.stream().sorted().limit(this.numberKeepHighScores)
                            .collect(Collectors.toList()));
        }
        try {
            this.saveEntitiesToFile(highScores);
        } catch (IOException ignored) {
        }
    }
}
