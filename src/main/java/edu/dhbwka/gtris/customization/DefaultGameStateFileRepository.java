package edu.dhbwka.gtris.customization;

import edu.dhbwka.gtris.model.application.AbstractFileRepository;
import edu.dhbwka.gtris.model.application.GameState;
import edu.dhbwka.gtris.model.application.IGameStateRepository;
import java.io.IOException;

public class DefaultGameStateFileRepository extends AbstractFileRepository<GameState>
        implements IGameStateRepository {

    public DefaultGameStateFileRepository(String filePathString) {
        super(filePathString);
    }

    @Override public GameState getGameState() {
        GameState gameState = null;
        try {
            gameState = loadEntitiesFromFile();
        } catch (ClassNotFoundException | IOException ignored) {
        }
        return gameState;
    }

    @Override public void saveGameState(final GameState gameState) {
        try {
            saveEntitiesToFile(gameState);
        } catch (IOException ignored) {
        }
    }
}
