package edu.dhbwka.gtris.customization;

import edu.dhbwka.gtris.model.application.IScoreEvaluator;

public class DefaultScoreEvaluator implements IScoreEvaluator {

    public DefaultScoreEvaluator() {}

    @Override public int getScoreForRows(int numRows, int level, int combo) {
        int score = getBaseRowValue(numRows);
        score *= switch (numRows) {
            case 0 -> 0;
            case 1 -> 100;
            case 2 -> 300;
            case 3 -> 500;
            case 4 -> 800;
            default -> throw new IllegalStateException("Unexpected value: " + numRows);
        };
        score *= level;
        score += 50 * combo * level;
        return score;
    }

    private int getBaseRowValue(int numRows) {
        return switch (numRows) {
            case 0 -> 0;
            case 1 -> 1;
            case 2 -> 3;
            case 3 -> 5;
            case 4 -> 8;
            default -> throw new IllegalStateException("Unexpected value: " + numRows);
        };
    }

    @Override public int getLevelBarrier(int level) {
        return 5 * level;
    }

    @Override public int getHardDropScore() {
        return 2;
    }

    @Override public int getSoftDropScore() {
        return 1;
    }

}
