package edu.dhbwka.gtris.model;

import edu.dhbwka.gtris.model.application.GameState;
import edu.dhbwka.gtris.model.application.IGameConfiguration;
import edu.dhbwka.gtris.model.application.IGameStateRepository;
import edu.dhbwka.gtris.model.application.IHighScoreRepository;
import edu.dhbwka.gtris.model.application.Mechanics;
import edu.dhbwka.gtris.model.application.entities.GTromino;
import edu.dhbwka.gtris.model.application.entities.IMino;
import edu.dhbwka.gtris.model.application.entities.IReadableScore;
import java.util.List;

public class GameController implements IGameController {

    private final IGameConfiguration gameConfiguration;
    private final IHighScoreRepository highScoreRepository;
    private final IGameStateRepository gameStateRepository;
    private final Mechanics mechanics;
    private GameState gameState;

    public GameController(final IGameConfiguration gameConfiguration) {
        this.gameConfiguration = gameConfiguration;
        highScoreRepository = this.gameConfiguration.getHighScoreRepository();
        gameStateRepository = this.gameConfiguration.getGameStateRepository();
        gameState = new GameState(this.gameConfiguration);
        mechanics = new Mechanics(gameState);
        gameConfiguration.registerGTrominos();
    }

    @Override public IGameConfiguration getGameConfiguration() {
        return gameConfiguration;
    }

    @Override public void start() {
        gameState.setPaused(false);
    }

    @Override public void update() {
        mechanics.stepOn();
    }

    @Override public void shiftLeft() {
        mechanics.shiftLeft();
    }

    @Override public void shiftRight() {
        mechanics.shiftRight();
    }

    @Override public void rotateClockwise() {
        mechanics.rotate(true);
    }

    @Override public void rotateCounterClockwise() {
        mechanics.rotate(false);
    }

    @Override public void hardDrop() {
        mechanics.executeHardDrop();
    }

    @Override public void softDrop() {
        mechanics.executeSoftDrop();
    }

    @Override public void hold() {
        mechanics.holdGtromino();
    }

    @Override public void pause() {
        gameState.setPaused(true);
    }

    @Override public boolean isPaused() {
        return gameState.isPaused();
    }

    @Override public boolean isGameOver() {
        return gameState.isGameOver();
    }

    @Override public List<IMino> getAllMinos() {
        return gameState.getMinos();
    }

    @Override public List<IMino> getGhostMinos() {
        return mechanics.getGhostMinos();
    }

    @Override public GTromino getHoldGtrominoType() {
        return gameState.getHoldBox();
    }

    @Override public GTromino[] getPreviewGtrominos(int count) {
        return gameState.getPreviewGtrominos(count);
    }

    @Override public IReadableScore getScore() {
        return gameState.getScore();
    }

    @Override public void saveHighScore(String playerName) {
        highScoreRepository.addHighScore(gameState.getScore(), playerName);
    }

    @Override public void saveGameState() {
        gameStateRepository.saveGameState(gameState);
    }

    @Override public void loadGameState() {
        GameState loadedGameState = gameStateRepository.getGameState();
        if (null != loadedGameState) {
            gameState = loadedGameState;
            mechanics.setGameState(gameState);
        }
    }
}
