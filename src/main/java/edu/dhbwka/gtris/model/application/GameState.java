package edu.dhbwka.gtris.model.application;

import edu.dhbwka.gtris.model.application.entities.DroppingGtromino;
import edu.dhbwka.gtris.model.application.entities.GTromino;
import edu.dhbwka.gtris.model.application.entities.IDroppingGtromino;
import edu.dhbwka.gtris.model.application.entities.IMino;
import edu.dhbwka.gtris.model.application.entities.IReadableScore;
import edu.dhbwka.gtris.model.application.entities.Playfield;
import edu.dhbwka.gtris.model.application.entities.primitives.Position;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class GameState implements Serializable {
    private final Playfield playfield;
    private final GTrominoQueue gTrominoQueue;
    private final ScoreManager scoreManager;
    private final IGameConfiguration gameConfiguration;
    private IDroppingGtromino droppingGtromino;
    private GTromino holdBox;
    private boolean gameOver;
    private boolean paused;
    private boolean pieceLockedOutOfBounds;

    public GameState(final IGameConfiguration gameConfiguration) {
        this.gameConfiguration = gameConfiguration;
        playfield = new Playfield(gameConfiguration.getPlayfieldWidth(),
                gameConfiguration.getPlayfieldHeight());
        gTrominoQueue = new GTrominoQueue(gameConfiguration.getPreviewCount(),3);
        scoreManager = new ScoreManager(gameConfiguration.getScoreEvaluator());
    }

    boolean isPieceLockedOutOfBounds() {
        return pieceLockedOutOfBounds;
    }

    void setPieceLockedOutOfBounds() {
        this.pieceLockedOutOfBounds = true;
    }

    ScoreManager getScoreManager() {
        return scoreManager;
    }

    void end() {
        this.gameOver = true;
    }

    public GTromino getHoldBox() {
        return holdBox;
    }

    void setHoldBox(final GTromino gtrominoType) {
        holdBox = gtrominoType;
    }

    GTrominoQueue getGtrominoQueue() {
        return gTrominoQueue;
    }

    public boolean isPaused() {
        return paused;
    }

    public void setPaused(final boolean isGamePaused) {
        this.paused = isGamePaused;
    }

    public boolean isGameOver() {
        return gameOver;
    }

    boolean isRunning() {
        return !this.paused && !this.gameOver;
    }

    int getPlayfieldWidth() {
        return this.playfield.getWidth();
    }

    int getPlayfieldHeight() {
        return this.playfield.getHeight();
    }

    int getDropInterval() {
        return this.gameConfiguration.getDropInterval(getScore().getLevel());
    }

    public IReadableScore getScore() {
        return this.scoreManager.getScore();
    }

    public List<IMino> getMinos() {
        final List<IMino> minos = new ArrayList<>(getGroundedMinos());
        final IDroppingGtromino activeGtromino = this.droppingGtromino;
        if (null != activeGtromino) {
            minos.addAll(activeGtromino.getMinos());
        }
        return minos;
    }

    List<IMino> getGroundedMinos() {
        return this.playfield.getMinos();
    }

    boolean move(final int x, final int y) {
        boolean result = false;
        if (null != droppingGtromino) {
            if (canMoveBy(x, y)) {
                droppingGtromino.moveBy(x, y);
                result = true;
            }
        }
        return result;
    }

    boolean canMoveBy(final int x, final int y) {
        boolean result = false;
        if (null != droppingGtromino) {
            result = canMoveTo(
                    droppingGtromino.getPosition().createRelativePosition(x, y));
        }
        return result;
    }

    private boolean canMoveTo(final Position p) {
        return canMoveTo(p.getX(), p.getY());
    }

    boolean canMoveTo(final int x, final int y) {
        for (final Position p : droppingGtromino.getMinoPositions(x, y)) {
            if (playfield.outOfBounds(p)) {return false;}
            for (final IMino mino : this.getGroundedMinos()) {
                if (p.collides(mino.getPosition())) {
                    return false;
                }
            }
        }
        return true;
    }

    IDroppingGtromino getDroppingGtromino() {
        return droppingGtromino;
    }

    void setDroppingGtromino(GTromino gtrominoType) {
        if (null != gtrominoType) {
            droppingGtromino = new DroppingGtromino(gtrominoType);
        } else {
            droppingGtromino = null;
        }
    }

    Playfield getPlayfield() {
        return playfield;
    }

    boolean canMoveBy(final Iterable<? extends IMino> minos, final Position parentPosition, final int x, final int y) {
        boolean result = true;
        for (final IMino mino1 : minos) {
            final int x1 = mino1.getPosition().getX();
            final int y1 = mino1.getPosition().getY();
            final Position virtualPosition = parentPosition.createRelativePosition(x1 + x, y1 + y);
            if (playfield.outOfBounds(virtualPosition)) {
                result = false;
                break;
            }
            for (final IMino mino : this.getGroundedMinos()) {
                if (virtualPosition.collides(mino.getPosition())) {
                    result = false;
                    break;
                }
            }
            if (!result) {break;}
        }
        return result;
    }

    public GTromino[] getPreviewGtrominos(final int count) {
        GTromino[] previewGTrominos = new GTromino[0];
        if (0 < count) {
            previewGTrominos = new GTromino[count];
            for (int index = 0; index < count; index++) {
                previewGTrominos[index] = this.gTrominoQueue.getPreviewGTrominos(index);
            }
        }
        return previewGTrominos;
    }

    public final IGameConfiguration getGameConfiguration() {
        return gameConfiguration;
    }

    public int getKickTries() {
        return droppingGtromino.getKickTries();
    }

    @Override public int hashCode() {
        return Objects.hash(playfield, this.gTrominoQueue, scoreManager,
                gameConfiguration, droppingGtromino, holdBox, gameOver,
                paused, pieceLockedOutOfBounds);
    }

    @Override public boolean equals(Object o) {
        if (this == o) {return true;}
        if (!(o instanceof final GameState gameState)) {return false;}
        return gameOver == gameState.gameOver && paused == gameState.paused &&
                       pieceLockedOutOfBounds == gameState.pieceLockedOutOfBounds &&
                       Objects.equals(playfield, gameState.playfield) &&
                       Objects.equals(this.gTrominoQueue, gameState.gTrominoQueue) &&
                       Objects.equals(scoreManager, gameState.scoreManager) &&
                       Objects.equals(gameConfiguration, gameState.gameConfiguration) &&
                       Objects.equals(droppingGtromino, gameState.droppingGtromino) &&
                       Objects.equals(holdBox, gameState.holdBox);
    }
}
