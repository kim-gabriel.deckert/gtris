package edu.dhbwka.gtris.model.application;

import edu.dhbwka.gtris.model.application.entities.GTromino;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.stream.Stream;

public class GTrominoQueue extends LinkedBlockingQueue<GTromino> {

    protected int previewCount = 3;

    public GTrominoQueue(final int previewCount, final int multiplier) {
        super(multiplier * previewCount);
        this.previewCount = previewCount;
        this.fillWithGTrominos();
    }

    public void fillWithGTrominos() {
        if (this.remainingCapacity() > GTrominoFactory.getNumAvailableProducts()) {
            final List<GTromino> gtrominos = GTrominoFactory.buildSeries();
            Collections.shuffle(gtrominos);
            Stream<GTromino> gTrominoStream = gtrominos.stream();
            gTrominoStream.forEach(element -> {
                try {
                    this.put(element);
                } catch (final InterruptedException e) {
                    throw new RuntimeException(e);
                }
            });
        }
    }

    public GTrominoQueue(final Collection<? extends GTromino> c) {
        super(c);
    }

    public GTrominoQueue() {
    }

    public GTromino pickGTromino() {
        if (this.size() <= previewCount * 2) {
            this.fillWithGTrominos();
        }
        return poll();
    }

    public GTromino getPreviewGTrominos(final int index) {
        GTromino result = null;
        if (0 <= index && index < this.size()) {
            Stream<GTromino> gTrominoStream = this.stream();
            List<GTromino> previewItems = gTrominoStream.toList();
            result = previewItems.get(index);
        } else {
            throw new IndexOutOfBoundsException(
                    "Index out of bounds. Index: " + index + ", Size: " + this.size());
        }
        return result;
    }

    public final int getGTrominoPreviewCount() {
        return this.previewCount;
    }
}
