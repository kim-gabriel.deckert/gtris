package edu.dhbwka.gtris.model.application.entities.primitives;

import java.io.Serializable;
import java.util.Objects;

public final class Position implements Serializable {
    private final int x;
    private final int y;

    public Position(final int x, final int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return this.x;
    }

    public int getY() {
        return this.y;
    }

    public Position createRelativePosition(final int x, final int y) {
        return new Position(this.x + x, this.y + y);
    }

    public boolean collides(final Position p) {
        return this.equals(p);
    }

    @Override public int hashCode() {
        return Objects.hash(this.x, this.y);
    }

    @Override public boolean equals(final Object o) {
        if (this == o) {return true;}
        if (!(o instanceof Position position)) {return false;}
        return this.x == position.x && this.y == position.y;
    }
}
