package edu.dhbwka.gtris.model.application.entities.primitives;


import java.io.Serializable;
import java.util.Objects;

public class MinoColor implements Serializable {
    private final int color;

    public MinoColor(final int r, final int g, final int b, final int a) {
        this.color = this.calculateColorValue(r, g, b, a);
    }

    private int calculateColorValue(final int r, final int g, final int b, final int a) {
        if (isValidColorParameter(r, g, b, a)) {
            throw new IllegalArgumentException("Invalid color values");
        }
        return (r << Channel.R.offset | g << Channel.G.offset | b << Channel.B.offset |
                        a << Channel.A.offset);
    }

    private static boolean isValidColorParameter(final int r, final int g, final int b, final int a) {
        return 0 > r || 255 < r || 0 > g || 255 < g || 0 > b || 255 < b || 0 > a || 255 < a;
    }

    public static MinoColor fromRGB(final int r, final int g, final int b, final int a) {
        return new MinoColor(r, g, b, a);
    }

    final int getColor() {
        return this.color;
    }

    public int[] getRgba() {
        return new int[]{ this.r(), this.g(), this.b(), this.a() };
    }

    private int r() {
        return this.get(Channel.R);
    }

    private int g() {
        return this.get(Channel.G);
    }

    private int b() {
        return this.get(Channel.B);
    }

    private int a() {
        return this.get(Channel.A);
    }

    private int get(final Channel channel) {
        return color >> channel.offset & 0xFF;
    }

    @Override public int hashCode() {
        return Objects.hashCode(color);
    }

    @Override public boolean equals(Object o) {
        if (this == o) {return true;}
        if (!(o instanceof final MinoColor minoColor)) {return false;}
        return color == minoColor.color;
    }

    private enum Channel {
        R(24),
        G(16),
        B(8),
        A(0);
        final int offset;

        Channel(final int offset) {
            this.offset = offset;
        }
    }
}
