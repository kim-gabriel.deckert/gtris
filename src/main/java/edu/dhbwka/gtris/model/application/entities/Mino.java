package edu.dhbwka.gtris.model.application.entities;

import edu.dhbwka.gtris.model.application.entities.primitives.MinoColor;
import edu.dhbwka.gtris.model.application.entities.primitives.Position;
import java.util.Objects;

public class Mino implements IMino {

    private final MinoColor color;
    private final char parentGTromino;
    private Position position;

    public Mino(int x, int y, char parentGTromino, MinoColor color) {
        position = new Position(x, y);
        this.parentGTromino = parentGTromino;
        this.color = color;
    }

    @Override public MinoColor getColor() {
        return color;
    }

    @Override public Position getPosition() {
        return position;
    }

    @Override public void moveTo(int x, int y) {
        position = new Position(x, y);
    }

    @Override public void moveBy(int x, int y) {
        position = position.createRelativePosition(x, y);
    }

    public void setPosition(final Position position) {
        this.position = position;
    }

    @Override public int hashCode() {
        return Objects.hash(this.color, this.parentGTromino, this.position);
    }

    @Override public boolean equals(final Object o) {
        if (this == o) {return true;}
        if (!(o instanceof Mino mino)) {return false;}
        return this.parentGTromino == mino.parentGTromino && Objects.equals(this.color, mino.color) &&
                       Objects.equals(this.position, mino.position);
    }
}
