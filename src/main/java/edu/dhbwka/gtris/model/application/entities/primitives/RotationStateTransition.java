package edu.dhbwka.gtris.model.application.entities.primitives;

import java.util.Arrays;
import java.util.Optional;

public enum RotationStateTransition {
    CLOCKWISE_0_90(RotationState.ROTATION_0, true),
    CLOCKWISE_90_180(RotationState.ROTATION_90, true),
    CLOCKWISE_180_270(RotationState.ROTATION_180, true),
    CLOCKWISE_270_0(RotationState.ROTATION_270, true),
    COUNTERCLOCKWISE_0_270(RotationState.ROTATION_0, false),
    COUNTERCLOCKWISE_270_180(RotationState.ROTATION_270, false),
    COUNTERCLOCKWISE_180_90(RotationState.ROTATION_180, false),
    COUNTERCLOCKWISE_90_0(RotationState.ROTATION_90, false);

    private final RotationState before;
    private final boolean clockwiseRotation;

    RotationStateTransition(RotationState before, boolean clockwiseRotation) {
        this.before = before;
        this.clockwiseRotation = clockwiseRotation;
    }

    public static RotationStateTransition getRotationStateTransition(
            RotationState rotationState, boolean clockwiseRotation) {
        Optional<RotationStateTransition> stateTransition =
                Arrays.stream(values())
                        .filter(rt -> rt.before == rotationState &&
                                              rt.clockwiseRotation == clockwiseRotation)
                        .findFirst();
        if (stateTransition.isPresent()) {
            return stateTransition.get();
        } else {
            throw new IllegalArgumentException();
        }
    }
}
