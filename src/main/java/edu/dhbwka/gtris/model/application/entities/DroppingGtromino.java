package edu.dhbwka.gtris.model.application.entities;

import edu.dhbwka.gtris.model.application.entities.primitives.MinoColor;
import edu.dhbwka.gtris.model.application.entities.primitives.Position;
import edu.dhbwka.gtris.model.application.entities.primitives.RotationState;
import edu.dhbwka.gtris.model.application.entities.primitives.RotationStateTransition;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class DroppingGtromino extends GTromino implements IDroppingGtromino {
    private final IMino[][] minos;
    private final GTromino gtromino;
    private Position position;
    private RotationState rotationState;
    private boolean unholdable;

    public DroppingGtromino(GTromino activeGtromino) {
        super(activeGtromino.getIdentifier(), activeGtromino.getColor(),
                activeGtromino.getKickData(), activeGtromino.getDimension(),
                activeGtromino.getMinoMatrix());
        gtromino = activeGtromino;
        this.minos = gtromino.createMinos();
        rotationState = RotationState.ROTATION_0;
        this.position = new Position(0, 0);
    }

    @Override public boolean isUnholdable() {
        return this.unholdable;
    }

    @Override public void setUnholdable(boolean isUnholdable) {
        unholdable = isUnholdable;
    }

    @Override public IMino[][] getRotatedClone(boolean clockwise) {
        IMino[][] result;
        if (clockwise) {
            result = this.rotateMatrixCounterClockwise(this.minos, getDimension(), false);
        } else {
            result = this.rotateMatrixClockwise(this.minos, getDimension(), false);
        }
        for (int x = 0; x < getDimension(); x++) {
            for (int y = 0; y < getDimension(); y++) {
                if (null != result[x][y]) {
                    result[x][y].moveTo(x, y);
                }
            }
        }
        return result;
    }

    IMino[][] rotateMatrixCounterClockwise(IMino[][] input, int dimension,
            boolean inplace) {
        IMino[][] output;
        if (inplace) {
            output = input;
        } else {
            output = Arrays.stream(input).map(IMino[]::clone).toArray(IMino[][]::new);
        }
        for (int i = 0; i < dimension / 2; i++) {
            for (int j = i; j < dimension - i - 1; j++) {
                IMino temp = input[i][j];
                output[i][j] = input[j][dimension - 1 - i];
                output[j][dimension - 1 - i] = input[dimension - 1 - i][dimension - 1 - j];
                output[dimension - 1 - i][dimension - 1 - j] = input[dimension - 1 - j][i];
                output[dimension - 1 - j][i] = temp;
            }
        }
        return output;
    }

    IMino[][] rotateMatrixClockwise(IMino[][] input, int dimension,
            boolean inplace) {
        IMino[][] output;
        if (inplace) {
            output = input;
        } else {
            output = Arrays.stream(input).map(IMino[]::clone).toArray(IMino[][]::new);
        }
        for (int i = 0; i < dimension / 2; i++) {
            for (int j = i; j < dimension - i - 1; j++) {
                IMino temp = input[i][j];
                output[i][j] = input[dimension - 1 - j][i];
                output[dimension - 1 - j][i] = input[dimension - 1 - i][dimension - 1 - j];
                output[dimension - 1 - i][dimension - 1 - j] = input[j][dimension - 1 - i];
                output[j][dimension - 1 - i] = temp;
            }
        }
        return output;
    }

    @Override public List<Position> getMinoPositions(int x, int y) {
        return Arrays.stream(this.minos).flatMap(Arrays::stream).filter(Objects::nonNull)
                       .map(mino -> mino.getPosition().createRelativePosition(x, y))
                       .collect(Collectors.toList());
    }

    @Override public void rotate(boolean clockwise) {
        if (clockwise) {
            this.rotateClockwise();
        } else {
            this.rotateCounterClockwise();
        }
        this.updateMinoPositions();
    }

    private void rotateClockwise() {
        int rotationStateIndex;
        rotationStateIndex = (rotationState.ordinal() + 1) % RotationState.values().length;
        rotationState = RotationState.values()[rotationStateIndex];
        this.rotateMatrixCounterClockwise(this.minos, getDimension(), true);
    }

    private void rotateCounterClockwise() {
        int rotationStateIndex;
        rotationStateIndex = (rotationState.ordinal() + RotationState.values().length - 1) %
                                     RotationState.values().length;
        rotationState = RotationState.values()[rotationStateIndex];
        this.rotateMatrixClockwise(this.minos, getDimension(), true);
    }

    private void updateMinoPositions() {
        for (int x = 0; x < getDimension(); x++) {
            for (int y = 0; y < getDimension(); y++) {
                if (null != this.minos[x][y]) {
                    this.minos[x][y].moveTo(x, y);
                }
            }
        }
    }

    @Override public Position getPosition() {
        return this.position;
    }

    @Override public void moveTo(int x, int y) {
        position = new Position(x, y);
    }

    @Override public void moveBy(int x, int y) {
        position = position.createRelativePosition(x, y);
    }

    @Override public RotationState getRotationState() {
        return this.rotationState;
    }

    public final void setRotationState(RotationState rotationState) {
        this.rotationState = rotationState;
    }

    @Override public List<IMino> getMinos() {
        if (null != this.position) {
            return this.getMinosAtPosition(this.position);
        } else {
            throw new IllegalArgumentException();
        }
    }

    @Override public List<IMino> getMinosAtPosition(Position p) {
        if (null != this.position) {
            final List<IMino> result = new ArrayList<>();
            for (int x = 0; x < this.getDimension(); x++) {
                for (int y = 0; y < this.getDimension(); y++) {
                    if (null != minos[x][y]) {
                        Position position1 = minos[x][y].getPosition();
                        int x1 = position1.getX();
                        int y1 = position1.getY();
                        final IMino mino =
                                new Mino(x1 + p.getX(), y1 + p.getY(), this.getIdentifier(),
                                        this.getColor());
                        result.add(mino);
                    }
                }
            }
            return result;
        } else {
            throw new IllegalArgumentException();
        }
    }

    @Override public GTromino getGtrominoType() {
        return gtromino;
    }

    public final void setPosition(Position position) {
        this.position = position;
    }

    @Override public int getKickTries() {
        return super.getKickTries();
    }

    @Override public int getKickX(final RotationStateTransition stateTransition, final int numberOfTries) {
        return super.getKickX(stateTransition, numberOfTries);
    }

    @Override public int getKickY(final RotationStateTransition stateTransition, final int numberOfTries) {
        return super.getKickY(stateTransition, numberOfTries);
    }

    @Override public MinoColor getColor() {
        return super.getColor();
    }

    @Override public boolean equals(Object o) {
        if (this == o) {return true;}
        if (!(o instanceof final DroppingGtromino that)) {return false;}
        if (!super.equals(o)) {return false;}
        return unholdable == that.unholdable &&
                       Objects.deepEquals(this.getMinos(), that.getMinos()) &&
                       Objects.equals(this.gtromino, that.gtromino) &&
                       Objects.equals(position, that.position) &&
                       rotationState == that.rotationState;
    }

    @Override public int hashCode() {
        return Objects.hash(super.hashCode(), this.getMinos(), this.gtromino, position, rotationState, unholdable);
    }
}
