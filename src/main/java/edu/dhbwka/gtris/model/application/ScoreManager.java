package edu.dhbwka.gtris.model.application;

import edu.dhbwka.gtris.model.application.entities.Score;
import java.io.Serializable;
import java.util.Objects;

class ScoreManager implements Serializable {

    private final Score score;
    private final IScoreEvaluator scoreEvaluator;
    private int combo;
    private int rowsInLevel;

    ScoreManager(IScoreEvaluator scoreEvaluator) {
        this.score = new Score();
        this.scoreEvaluator = scoreEvaluator;
    }

    public Score getScore() {
        return this.score;
    }

    void processRows(int numRows) {
        if (0 <= numRows) {
            if (0 == numRows) {
                combo = 0;
            }
            if (0 < numRows) {
                this.addRowsCleared(numRows);
            }
        }
    }

    private void addRowsCleared(int numRows) {
        if (0 < numRows) {
            int levelBarrier = scoreEvaluator.getLevelBarrier(score.getLevel());
            int scoreAddendum =
                    scoreEvaluator.getScoreForRows(numRows, score.getLevel(), combo);
            score.addScore(scoreAddendum);
            score.addToRowCounter(numRows);
            rowsInLevel = rowsInLevel + numRows;
            if (rowsInLevel >= levelBarrier) {
                score.upLevel();
                rowsInLevel = 0;
            }
            combo = combo + 1;
        }

    }

    void addHardDropScore(int distance) {
        int scoreAddendum = scoreEvaluator.getHardDropScore() * distance;
        score.addScore(scoreAddendum);
    }

    void addSoftDropScore() {
        int scoreAddendum = scoreEvaluator.getSoftDropScore();
        score.addScore(scoreAddendum);
    }

    private boolean same(ScoreManager that) {
        return this.combo == that.combo && this.rowsInLevel == that.rowsInLevel &&
                       Objects.equals(this.score, that.score) &&
                       Objects.equals(this.scoreEvaluator, that.scoreEvaluator);
    }

    @Override public int hashCode() {
        return Objects.hash(this.score, scoreEvaluator, combo, rowsInLevel);
    }

    @Override public boolean equals(final Object o) {
        if (this == o) {return true;}
        if (!(o instanceof ScoreManager that)) {return false;}
        return combo == that.combo && rowsInLevel == that.rowsInLevel &&
                       Objects.equals(this.score, that.score) &&
                       Objects.equals(scoreEvaluator, that.scoreEvaluator);
    }
}
