package edu.dhbwka.gtris.model.application.entities;

import java.io.Serializable;
import java.util.Objects;

public class Score implements IReadableScore, Serializable {
    private int score;
    private int level;
    private int rows;

    public Score() {
        score = 0;
        level = 0;
        rows = 0;
    }

    public void addScore(int addendum) {
        if (0 < addendum) {
            score = score + addendum;
        }
    }

    public void upLevel() {
        level = level + 1;
    }

    public void addToRowCounter(int addendum) {
        if (0 < addendum) {
            rows = rows + addendum;
        }
    }

    @Override public int getScore() {
        return this.score;
    }

    public void setScore(final int score) {
        this.score = score;
    }

    @Override public int getLevel() {
        return this.level;
    }

    @Override public int getRows() {
        return this.rows;
    }

    @Override public int compareTo(IReadableScore o) {
        return Integer.compare(score, o.getScore());
    }

    @Override public int hashCode() {
        return Objects.hash(score, level, rows);
    }

    @Override public boolean equals(Object o) {
        if (this == o) {return true;}
        if (!(o instanceof final Score score1)) {return false;}
        return score == score1.score && level == score1.level && rows == score1.rows;
    }
}
