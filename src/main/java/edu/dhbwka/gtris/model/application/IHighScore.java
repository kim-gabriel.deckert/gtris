package edu.dhbwka.gtris.model.application;

import edu.dhbwka.gtris.model.application.entities.IReadableScore;
import java.io.Serializable;
import java.time.LocalDateTime;

public interface IHighScore extends IReadableScore, Serializable {
    @Override int getScore();

    @Override int getLevel();

    @Override int getRows();

    String getName();

    long getTimestamp();

    LocalDateTime getDateTime();

}
