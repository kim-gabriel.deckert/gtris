package edu.dhbwka.gtris.model.application.entities;

import edu.dhbwka.gtris.model.application.entities.primitives.IMovable;
import edu.dhbwka.gtris.model.application.entities.primitives.MinoColor;
import edu.dhbwka.gtris.model.application.entities.primitives.Position;
import java.io.Serializable;

public interface IMino extends IMovable, Serializable {
    MinoColor getColor();

    @Override Position getPosition();

    @Override void moveTo(int x, int y);

    @Override void moveBy(int x, int y);

}
