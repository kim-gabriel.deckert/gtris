package edu.dhbwka.gtris.model.application.entities;

import edu.dhbwka.gtris.model.application.entities.primitives.IMovable;
import edu.dhbwka.gtris.model.application.entities.primitives.MinoColor;
import edu.dhbwka.gtris.model.application.entities.primitives.Position;
import edu.dhbwka.gtris.model.application.entities.primitives.RotationState;
import edu.dhbwka.gtris.model.application.entities.primitives.RotationStateTransition;
import java.io.Serializable;
import java.util.List;

public interface IDroppingGtromino extends IMovable, Serializable {
    boolean isUnholdable();
    void setUnholdable(boolean unholdable);
    IMino[][] getRotatedClone(boolean clockwise);
    List<Position> getMinoPositions(int x, int y);
    void rotate(boolean clockwise);
    Position getPosition();
    @Override void moveTo(int x, int y);
    @Override void moveBy(int x, int y);
    RotationState getRotationState();
    List<IMino> getMinos();
    List<IMino> getMinosAtPosition(Position p);
    GTromino getGtrominoType();
    int getKickTries();
    MinoColor getColor();
    int getKickX(RotationStateTransition stateTransition, int numberOfTries);
    int getKickY(RotationStateTransition stateTransition, int numberOfTries);
}
