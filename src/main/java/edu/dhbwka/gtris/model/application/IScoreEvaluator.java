package edu.dhbwka.gtris.model.application;

import java.io.Serializable;

public interface IScoreEvaluator extends Serializable {
    int getScoreForRows(int numRows, int level, int combo);

    int getLevelBarrier(int level);

    int getHardDropScore();

    int getSoftDropScore();
}
