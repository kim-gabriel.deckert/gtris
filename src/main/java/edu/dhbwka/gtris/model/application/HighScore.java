package edu.dhbwka.gtris.model.application;

import edu.dhbwka.gtris.model.application.entities.IReadableScore;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.Objects;
import java.util.TimeZone;

public final class HighScore implements IHighScore {

    private final String name;
    private final long timestamp;
    private final int score;
    private final int level;
    private final int rows;

    public HighScore(IReadableScore readableScore, String playerName) {
        if (HighScore.validate(readableScore, playerName)) {
            score = readableScore.getScore();
            level = readableScore.getLevel();
            rows = readableScore.getRows();
            name = playerName;
            timestamp = Instant.now().getEpochSecond();
        } else {
            throw new IllegalArgumentException(
                    "Invalid score or player name. The score must be greater than or equal to 0, the level must be greater than or equal to 0, the rows must be greater than or equal to 0, and the player name must not be blank.");
        }
    }

    private static boolean validate(final IReadableScore score, final String name) {
        return 0 <= score.getScore() && 0 <= score.getLevel() && 0 <= score.getRows() &&
                       !name.isBlank();
    }

    @Override public int getScore() {
        return score;
    }

    @Override public int getLevel() {
        return level;
    }

    @Override public int getRows() {
        return rows;
    }

    @Override public String getName() {
        return name;
    }

    @Override public long getTimestamp() {
        return timestamp;
    }

    @Override public LocalDateTime getDateTime() {
        ZonedDateTime zonedDateTime =
                Instant.ofEpochSecond(timestamp).atZone(TimeZone.getDefault().toZoneId());
        return zonedDateTime.toLocalDateTime();
    }

    @Override public int compareTo(IReadableScore o) {
        return Integer.compare(o.getScore(), score);
    }

    private boolean same(HighScore highScore) {
        return this.timestamp == highScore.timestamp && this.score == highScore.score &&
                       this.level == highScore.level && this.rows == highScore.rows &&
                       Objects.equals(this.name, highScore.name);
    }

    @Override public int hashCode() {
        return Objects.hash(this.name, this.timestamp, this.score, this.level, this.rows);
    }

    @Override public boolean equals(final Object o) {
        if (this == o) {return true;}
        if (!(o instanceof HighScore highScore)) {return false;}
        return this.timestamp == highScore.timestamp && this.score == highScore.score &&
                       this.level == highScore.level && this.rows == highScore.rows &&
                       Objects.equals(this.name, highScore.name);
    }
}
