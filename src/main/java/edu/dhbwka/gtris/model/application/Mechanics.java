package edu.dhbwka.gtris.model.application;

import edu.dhbwka.gtris.model.application.entities.GTromino;
import edu.dhbwka.gtris.model.application.entities.IDroppingGtromino;
import edu.dhbwka.gtris.model.application.entities.IMino;
import edu.dhbwka.gtris.model.application.entities.Mino;
import edu.dhbwka.gtris.model.application.entities.Playfield;
import edu.dhbwka.gtris.model.application.entities.primitives.Position;
import edu.dhbwka.gtris.model.application.entities.primitives.RotationState;
import edu.dhbwka.gtris.model.application.entities.primitives.RotationStateTransition;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

public class Mechanics {

    private GameState gameState;
    private long lastDrop;

    public Mechanics(final GameState gameState) {
        if (null != gameState) {this.gameState = gameState;} else {
            throw new IllegalArgumentException();
        }
    }

    public void executeSoftDrop() {
        if (null != this.gameState.getDroppingGtromino() && this.gameState.isRunning()) {
            if (gravity()) {
                final ScoreManager scoreManager = this.gameState.getScoreManager();
                scoreManager.addSoftDropScore();
            }
        }
    }

    private boolean gravity() {
        boolean result = false;
        if (null != this.gameState.getDroppingGtromino()) {
            final boolean moved = this.gameState.move(0, 1);
            if (!moved) {
                atomizeGtromino();
            }
            result = moved;
        }
        return result;
    }


    private void atomizeGtromino() {
        final Playfield playfield = this.gameState.getPlayfield();
        final IDroppingGtromino droppingGtromino = this.gameState.getDroppingGtromino();
        playfield.getMinos().addAll(droppingGtromino.getMinos());
        final Map<Integer, List<IMino>> fullRows = playfield.getFullRows();
        if (!fullRows.isEmpty()) {
            final int rowsCleared = fullRows.keySet().size();
            this.gameState.getScoreManager().processRows(rowsCleared);
        }
        this.gameState.setDroppingGtromino(null);
    }


    public void executeHardDrop() {
        if (this.gameState.isRunning()) {
            int distance = 0;
            while (null != this.gameState.getDroppingGtromino()) {
                if (gravity()) {
                    distance++;
                }
            }
            final ScoreManager scoreManager = this.gameState.getScoreManager();
            scoreManager.addHardDropScore(distance);
        }
    }

    public void rotate(final boolean clockwise) {
        if (this.gameState.isRunning()) {
            final IDroppingGtromino droppingGtromino = this.gameState.getDroppingGtromino();
            if (null != droppingGtromino && this.gameState.move(0, 0)) {
                for (int index = 0; index < this.gameState.getKickTries(); index++) {
                    final List<IMino> virtualMinos =
                            Arrays.stream(droppingGtromino.getRotatedClone(clockwise))
                                    .flatMap(Arrays::stream).filter(Objects::nonNull)
                                    .collect(Collectors.toList());
                    final RotationState currentRotationState = droppingGtromino.getRotationState();
                    final RotationStateTransition stateTransition =
                            RotationStateTransition.getRotationStateTransition(currentRotationState,
                                    clockwise);
                    final int kickX = droppingGtromino.getKickX(stateTransition, index);
                    final int kickY = droppingGtromino.getKickY(stateTransition, index);
                    if (this.gameState.canMoveBy(virtualMinos, droppingGtromino.getPosition(), kickX,
                            kickY) && this.gameState.move(kickX, kickY)) {
                        droppingGtromino.rotate(clockwise);
                        break;
                    }
                }
            }
        }
    }

    public void shiftLeft() {
        shift(false);
    }

    private void shift(final boolean right) {
        if (gameState.isRunning()) {
            final int y = 0;
            final int x = right ? 1 : -1;
            if (this.gameState.canMoveBy(x, y)) {
                this.gameState.move(x, y);
            }
        }
    }

    public void shiftRight() {
        shift(true);
    }

    public List<IMino> getGhostMinos() {
        final List<IMino> ghostMinos = new ArrayList<>();
        final IDroppingGtromino droppingGtromino = gameState.getDroppingGtromino();
        if (null != droppingGtromino) {
            final int maxMoveY = getMaxMoveY();
            final GTromino gTromino = droppingGtromino.getGtrominoType();
            if (maxMoveY > gTromino.getDimension()) {
                for (final IMino mino : droppingGtromino.getMinos()) {
                    final Position ghostPosition = mino.getPosition().createRelativePosition(0, maxMoveY);
                    final IMino ghostMino = new Mino(ghostPosition.getX(), ghostPosition.getY(),
                            gTromino.getIdentifier(), droppingGtromino.getColor());
                    ghostMinos.add(ghostMino);
                }
            }
        }
        return ghostMinos;
    }

    private int getMaxMoveY() {
        final Collection<Integer> possibleYmoves = new HashSet<>();
        final IDroppingGtromino droppingGtromino = gameState.getDroppingGtromino();
        for (final IMino mino : droppingGtromino.getMinos()) {
            final List<IMino> groundedMinos = gameState.getGroundedMinos();
            final List<IMino> columnMinos = groundedMinos.stream().filter(gmino -> {
                final Position position = gmino.getPosition();
                final Position minoPosition = mino.getPosition();
                return position.getX() == minoPosition.getX() &&
                               position.getY() >= minoPosition.getY();
            }).toList();
            final Optional<Integer> maxMoveYDifference = columnMinos.stream().map(IMino::getPosition)
                                                           .map(position -> Integer.valueOf(
                                                                   position.getY() -
                                                                           mino.getPosition()
                                                                                   .getY()))
                                                           .min(Integer::compareTo);
            if (maxMoveYDifference.isPresent()) {
                possibleYmoves.add(maxMoveYDifference.get() - 1);
            } else {
                possibleYmoves.add(
                        gameState.getPlayfieldHeight() - mino.getPosition().getY() - 1);
            }
        }
        final Optional<Integer> minPossibleYmove = possibleYmoves.stream().min(Integer::compareTo);
        int result = 0;
        if (minPossibleYmove.isPresent()) {
            final Integer minYMoveValue = minPossibleYmove.get();
            if (0 <= minYMoveValue.intValue() ||
                        minYMoveValue.intValue() < gameState.getPlayfieldHeight()) {
                result = minYMoveValue.intValue();
            }
        }
        return result;
    }

    public void stepOn() {
        if (gameState.isRunning()) {
            if (null == gameState.getDroppingGtromino()) {
                dropNextGtromino();
            } else if (System.currentTimeMillis() - lastDrop >
                               gameState.getDropInterval()) {
                gravity();
                lastDrop = System.currentTimeMillis();
            }
        }
    }

    private void dropNextGtromino() {
        final GTromino nextGtrominoType = gameState.getGtrominoQueue().pickGTromino();
        dropGTromino(nextGtrominoType);
        if (gameState.isPieceLockedOutOfBounds()) {
            gameState.end();
        }
    }

    private void dropGTromino(final GTromino gtrominoType) {
        gameState.setDroppingGtromino(gtrominoType);
        final int xOrigin = (int) Math.floor((gtrominoType.getDimension() / 2.0f));
        final int x = (int) Math.floor((this.gameState.getPlayfieldWidth() / 2.0f)) - xOrigin;
        final int y = -gtrominoType.getDimension();
        if (this.gameState.canMoveTo(x, y)) {
            final IDroppingGtromino droppingGtromino = gameState.getDroppingGtromino();
            droppingGtromino.moveTo(x, y);
        } else {
            gameState.setPieceLockedOutOfBounds();
            gameState.setDroppingGtromino(null);
        }
    }

    public void holdGtromino() {
        if (gameState.isRunning()) {
            IDroppingGtromino droppingGtromino = gameState.getDroppingGtromino();
            if (null != droppingGtromino && !droppingGtromino.isUnholdable()) {
                final GTromino heldGTromino = gameState.getHoldBox();
                gameState.setHoldBox(droppingGtromino.getGtrominoType());
                if (null == heldGTromino) {
                    dropNextGtromino();
                } else {
                    this.dropGTromino(heldGTromino);
                }
                droppingGtromino = gameState.getDroppingGtromino();
                droppingGtromino.setUnholdable(true);
            }
        }
    }

    public final void setGameState(final GameState state) {
        this.gameState = state;
    }

}
