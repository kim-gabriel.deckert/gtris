package edu.dhbwka.gtris.model.application.entities.primitives;

public interface IMovable {
    Position getPosition();

    void moveTo(int x, int y);

    void moveBy(int x, int y);
}
