package edu.dhbwka.gtris.model.application;

import java.io.Serializable;

public interface IGameConfiguration extends Serializable {
    int getPlayfieldWidth();

    int getPlayfieldHeight();

    int getPreviewCount();

    int getDropInterval(int level);

    IScoreEvaluator getScoreEvaluator();

    IHighScoreRepository getHighScoreRepository();

    IGameStateRepository getGameStateRepository();

    void registerGTrominos();
}
