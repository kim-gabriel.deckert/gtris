package edu.dhbwka.gtris.model.application;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;

public abstract class AbstractFileRepository<W extends Serializable> {
    private final String filePathString;

    protected AbstractFileRepository(final String filePathString) {
        this.filePathString = filePathString;
    }

    protected W loadEntitiesFromFile() throws ClassNotFoundException, IOException {
        W entities = null;
        final Path filePath = Path.of(this.filePathString);
        if (Files.exists(filePath)) {
            try (final InputStream inputStream = Files.newInputStream(
                    filePath); final ObjectInputStream objectInputStream = new ObjectInputStream(
                    inputStream)) {
                entities = (W) objectInputStream.readObject();
            } catch (final ClassNotFoundException | IOException e) {
                throw e;
            }
        }
        return entities;
    }

    protected void saveEntitiesToFile(final W entities) throws IOException {
        final Path filePath = Path.of(this.filePathString);
        if (!Files.exists(filePath.getParent())) {
            try {
                Files.createDirectories(filePath.getParent());
            } catch (final IOException e) {
                throw e;
            }
        }
        try (final OutputStream outputStream = Files.newOutputStream(
                filePath); final ObjectOutput objectOutputStream = new ObjectOutputStream(outputStream)) {

            objectOutputStream.writeObject(entities);
        } catch (final IOException e) {
            throw e;
        }
    }
}
