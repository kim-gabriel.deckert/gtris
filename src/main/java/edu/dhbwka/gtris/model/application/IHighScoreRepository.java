package edu.dhbwka.gtris.model.application;

import edu.dhbwka.gtris.model.application.entities.IReadableScore;
import java.util.ArrayList;

public interface IHighScoreRepository {
    ArrayList<IHighScore> getHighScores();
    void addHighScore(IReadableScore score, String playerName);
}
