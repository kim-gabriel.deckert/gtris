package edu.dhbwka.gtris.model.application.entities;

import edu.dhbwka.gtris.model.application.entities.primitives.MinoColor;
import edu.dhbwka.gtris.model.application.entities.primitives.RotationStateTransition;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.EnumMap;
import java.util.Objects;
import java.util.OptionalInt;
import java.util.stream.Stream;

public abstract class GTromino implements Serializable {
    private final char identifier;

    EnumMap<RotationStateTransition, int[][]> getKickData() {
        return this.kickData;
    }

    boolean[][] getMinoMatrix() {
        return this.minoMatrix;
    }

    private final EnumMap<RotationStateTransition, int[][]> kickData;
    private final int dimension;
    private final boolean[][] minoMatrix;
    private final MinoColor color;

    protected GTromino(char identifier, MinoColor color,
            EnumMap<RotationStateTransition, int[][]> kickData, int dimension,
            boolean[][] minoMatrix) {
        super();
        this.identifier = identifier;
        this.color = color;
        this.kickData = kickData;
        this.dimension = dimension;
        this.minoMatrix = minoMatrix;
    }

    public int getKickTries() {
        Collection<int[][]> kickDataValuesList = kickData.values();
        Stream<int[][]> stream = kickDataValuesList.stream();
        final OptionalInt maxTries = stream.mapToInt(mt -> mt.length).min();
        if (maxTries.isPresent()) {return maxTries.getAsInt();} else {return 0;}
    }

    public int getKickX(final RotationStateTransition stateTransition, final int numberOfTries) {
        return getKick(stateTransition, numberOfTries)[0];
    }

    private int[] getKick(RotationStateTransition stateTransition, int numberOfTries) {
        if (kickData.containsKey(stateTransition) && 0 <= numberOfTries &&
                    numberOfTries < kickData.get(stateTransition).length) {
            return kickData.get(stateTransition)[numberOfTries];
        }
        throw new IllegalArgumentException();
    }

    public int getKickY(final RotationStateTransition stateTransition, final int numberOfTries) {
        return -1 * getKick(stateTransition, numberOfTries)[1];
    }

    public int getDimension() {
        return this.dimension;
    }

    public Mino[][] createMinos() {
        Mino[][] minos = new Mino[this.dimension][this.dimension];
        for (int x = 0; x < this.dimension; x++) {
            for (int y = 0; y < this.dimension; y++) {
                if (minoMatrix[x][y]) {
                    minos[x][y] = new Mino(x, y, this.identifier, this.color);
                }
            }
        }
        return minos;
    }

    public char getIdentifier() {
        return this.identifier;
    }

    public MinoColor getColor() {
        return this.color;
    }

    private boolean same(GTromino gTromino) {
        return this.identifier == gTromino.identifier && this.dimension == gTromino.dimension &&
                       Objects.equals(kickData, gTromino.kickData) &&
                       Objects.deepEquals(minoMatrix, gTromino.minoMatrix) &&
                       Objects.equals(this.color, gTromino.color);
    }

    @Override public int hashCode() {
        return Objects.hash(this.identifier, kickData, this.dimension,
                Arrays.deepHashCode(minoMatrix), this.color);
    }

    @Override public boolean equals(final Object o) {
        if (this == o) {return true;}
        if (!(o instanceof GTromino gTromino)) {return false;}
        return this.identifier == gTromino.identifier && this.dimension == gTromino.dimension &&
                       Objects.equals(kickData, gTromino.kickData) &&
                       Objects.deepEquals(minoMatrix, gTromino.minoMatrix) &&
                       Objects.equals(this.color, gTromino.color);
    }
}
