package edu.dhbwka.gtris.model.application;

import edu.dhbwka.gtris.model.application.entities.DefaultGTrominos;
import edu.dhbwka.gtris.model.application.entities.GTromino;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

public final class GTrominoFactory {
    private final static Map<Character, Supplier<? extends GTromino>> availableProducts =
            new HashMap<>();
    static {
        registerDefaultGTrominos();
    }

    private GTrominoFactory() {
    }

    public static GTromino buildGTromino(final char type) {
        if (availableProducts.containsKey(type)) {
            final Supplier<? extends GTromino> supplier = GTrominoFactory.availableProducts.get(type);
            return supplier.get();
        } else {
            throw new IllegalArgumentException("The GTromino type: " + type + " is not available.");
        }
    }

    public static int getNumAvailableProducts() {
        return GTrominoFactory.availableProducts.size();
    }

    public static List<GTromino> buildSeries() {
        final List<GTromino> gTrominos = new ArrayList<>();
        for (Object supplier : GTrominoFactory.availableProducts.values()) {
            gTrominos.add(((Supplier<? extends GTromino>) supplier).get());
        }
        return gTrominos;
    }

    static void registerDefaultGTrominos() {
        GTrominoFactory.register('I', DefaultGTrominos.I::new);
        GTrominoFactory.register('J', DefaultGTrominos.J::new);
        GTrominoFactory.register('L', DefaultGTrominos.L::new);
        GTrominoFactory.register('O', DefaultGTrominos.O::new);
        GTrominoFactory.register('S', DefaultGTrominos.S::new);
        GTrominoFactory.register('T', DefaultGTrominos.T::new);
        GTrominoFactory.register('Z', DefaultGTrominos.Z::new);
    }

    public static void register(char identifier, final Supplier<? extends GTromino> supplier) {
        availableProducts.put(identifier, supplier);
    }

    public Map<Character, Supplier<? extends GTromino>> getAvailableProducts() {
        return availableProducts;
    }

}
