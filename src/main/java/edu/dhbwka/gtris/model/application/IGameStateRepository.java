package edu.dhbwka.gtris.model.application;

public interface IGameStateRepository {
    GameState getGameState();
    void saveGameState(GameState gameState);
}
