package edu.dhbwka.gtris.model.application.entities;

public interface IReadableScore extends Comparable<IReadableScore> {
    int getScore();

    int getLevel();

    int getRows();
}
