package edu.dhbwka.gtris.model.application.entities;

import edu.dhbwka.gtris.model.application.entities.primitives.MinoColor;
import edu.dhbwka.gtris.model.application.entities.primitives.RotationStateTransition;
import java.util.EnumMap;

public enum DefaultGTrominos {
    ;
    private static final EnumMap<RotationStateTransition, int[][]> kickData23 =
            new EnumMap<>(RotationStateTransition.class) {
                {
                    this.put(RotationStateTransition.CLOCKWISE_0_90,
                            new int[][]{ { 0, 0 }, { -1, 0 }, { -1, 1 }, { 0, -2 }, { -1, -2 } });
                    this.put(RotationStateTransition.CLOCKWISE_90_180,
                            new int[][]{ { 0, 0 }, { 1, 0 }, { 1, -1 }, { 0, 2 }, { 1, 2 } });
                    this.put(RotationStateTransition.CLOCKWISE_180_270,
                            new int[][]{ { 0, 0 }, { 1, 0 }, { 1, 1 }, { 0, -2 }, { 1, -2 } });
                    this.put(RotationStateTransition.CLOCKWISE_270_0,
                            new int[][]{ { 0, 0 }, { -1, 0 }, { -1, -1 }, { 0, 2 }, { -1, 2 } });
                    this.put(RotationStateTransition.COUNTERCLOCKWISE_0_270,
                            new int[][]{ { 0, 0 }, { 1, 0 }, { 1, 1 }, { 0, -2 }, { 1, -2 } });
                    this.put(RotationStateTransition.COUNTERCLOCKWISE_270_180,
                            new int[][]{ { 0, 0 }, { -1, 0 }, { -1, -1 }, { 0, 2 }, { -1, 2 } });
                    this.put(RotationStateTransition.COUNTERCLOCKWISE_180_90,
                            new int[][]{ { 0, 0 }, { -1, 0 }, { -1, 1 }, { 0, -2 }, { -1, -2 } });
                    this.put(RotationStateTransition.COUNTERCLOCKWISE_90_0,
                            new int[][]{ { 0, 0 }, { 1, 0 }, { 1, -1 }, { 0, 2 }, { 1, 2 } });
                }
            };
    private static final EnumMap<RotationStateTransition, int[][]> kickDataD04 =
            new EnumMap<>(RotationStateTransition.class) {{
                this.put(RotationStateTransition.CLOCKWISE_0_90,
                        new int[][]{ { 0, 0 }, { -2, 0 }, { 1, 0 }, { -2, -1 }, { 1, 2 } });
                this.put(RotationStateTransition.CLOCKWISE_90_180,
                        new int[][]{ { 0, 0 }, { -1, 0 }, { 2, 0 }, { -1, 2 }, { 2, -1 } });
                this.put(RotationStateTransition.CLOCKWISE_180_270,
                        new int[][]{ { 0, 0 }, { 2, 0 }, { -1, 0 }, { 2, 1 }, { -1, -2 } });
                this.put(RotationStateTransition.CLOCKWISE_270_0,
                        new int[][]{ { 0, 0 }, { 1, 0 }, { -2, 0 }, { 1, -2 }, { -2, 1 } });
                this.put(RotationStateTransition.COUNTERCLOCKWISE_0_270,
                        new int[][]{ { 0, 0 }, { -1, 0 }, { 2, 0 }, { -1, 2 }, { 2, -1 } });
                this.put(RotationStateTransition.COUNTERCLOCKWISE_270_180,
                        new int[][]{ { 0, 0 }, { -2, 0 }, { 1, 0 }, { -2, -1 }, { 1, 2 } });
                this.put(RotationStateTransition.COUNTERCLOCKWISE_180_90,
                        new int[][]{ { 0, 0 }, { 1, 0 }, { -2, 0 }, { 1, -2 }, { -2, 1 } });
                this.put(RotationStateTransition.COUNTERCLOCKWISE_90_0,
                        new int[][]{ { 0, 0 }, { 2, 0 }, { -1, 0 }, { 2, 1 }, { -1, -2 } });
            }};

    public static class I extends GTromino {
        public I() {
            super('I', MinoColor.fromRGB(0, 192, 192, 255), DefaultGTrominos.kickDataD04, 4,
                    new boolean[][]{ { false, true, false, false }, { false, true, false, false },
                            { false, true, false, false }, { false, true, false, false } });

        }
    }
    public static class J extends GTromino {
        public J() {
            super('J', MinoColor.fromRGB(0, 0, 192, 255), DefaultGTrominos.kickData23, 3,
                    new boolean[][]{ { true, true, false }, { false, true, false },
                            { false, true, false } });

        }
    }

    public static class L extends GTromino {
        public L() {
            super('L', MinoColor.fromRGB(192, 128, 0, 255), DefaultGTrominos.kickData23, 3,
                    new boolean[][]{ { false, true, false }, { false, true, false },
                            { true, true, false } });

        }
    }

    public static class O extends GTromino {
        public O() {
            super('O', MinoColor.fromRGB(192, 192, 0, 255), DefaultGTrominos.kickData23, 2,
                    new boolean[][]{ { true, true }, { true, true } });

        }
    }

    public static class S extends GTromino {
        public S() {
            super('S', MinoColor.fromRGB(0, 192, 0, 255), DefaultGTrominos.kickData23, 3,
                    new boolean[][]{ { false, true, false }, { true, true, false },
                            { true, false, false } });

        }
    }

    public static class T extends GTromino {
        public T() {
            super('T', MinoColor.fromRGB(128, 0, 192, 255), DefaultGTrominos.kickData23, 3,
                    new boolean[][]{ { false, true, false }, { true, true, false },
                            { false, true, false } });

        }
    }

    public static class Z extends GTromino {
        public Z() {
            super('Z', MinoColor.fromRGB(192, 0, 0, 255), DefaultGTrominos.kickData23, 3,
                    new boolean[][]{ { true, false, false }, { true, true, false },
                            { false, true, false } });

        }
    }
}
