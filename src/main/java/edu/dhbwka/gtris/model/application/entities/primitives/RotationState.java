package edu.dhbwka.gtris.model.application.entities.primitives;

public enum RotationState {
    ROTATION_0,
    ROTATION_90,
    ROTATION_180,
    ROTATION_270
}
