package edu.dhbwka.gtris.model.application.entities;

import edu.dhbwka.gtris.model.application.entities.primitives.Position;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public class Playfield implements Serializable {

    private final List<IMino> minos = new ArrayList<>();
    private final int width;
    private final int height;

    public Playfield(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public Map<Integer, List<IMino>> getFullRows() {
        HashMap<Integer, List<IMino>> rows = new HashMap<>();

        for (int y = 0; y < this.height; y++) {
            final int rowNumber = y;
            Set<Position> rowPositions =
                    minos.stream().filter(Objects::nonNull).map(IMino::getPosition)
                            .filter(p -> p.getY() == rowNumber).collect(Collectors.toSet());
            if (rowPositions.size() == this.width) {
                List<IMino> row = new ArrayList<>();
                for (Position position : rowPositions) {
                    Optional<IMino> mino = minos.stream().toList().stream()
                                                         .filter(mino1 -> null != mino1 &&
                                                                                  mino1.getPosition()
                                                                                          .equals(position))
                                                         .findFirst();
                    if (mino.isPresent()) {
                        row.add(mino.get());
                    } else {
                        throw new IllegalArgumentException();
                    }
                }
                minos.removeAll(row);
                minos.stream()
                        .filter(mino -> null != mino && mino.getPosition().getY() < rowNumber)
                        .forEach(mino -> mino.moveBy(0, 1));
                rows.put(y, row);
            }
        }
        return rows;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    public boolean outOfBounds(Position p) {
        return 0 > p.getX() || p.getX() >= this.width || p.getY() >= this.height;
    }

    @Override public int hashCode() {
        return Objects.hash(this.minos, width, height);
    }

    @Override public boolean equals(Object o) {
        if (this == o) {return true;}
        if (!(o instanceof final Playfield playfield)) {return false;}
        return width == playfield.width && height == playfield.height &&
                       Objects.equals(this.minos, playfield.minos);
    }

    public List<IMino> getMinos() {
        return minos;
    }
}
