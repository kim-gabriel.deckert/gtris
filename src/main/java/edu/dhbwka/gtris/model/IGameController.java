package edu.dhbwka.gtris.model;

import edu.dhbwka.gtris.model.application.IGameConfiguration;
import edu.dhbwka.gtris.model.application.entities.GTromino;
import edu.dhbwka.gtris.model.application.entities.IMino;
import edu.dhbwka.gtris.model.application.entities.IReadableScore;
import java.util.List;

public interface IGameController {
    IGameConfiguration getGameConfiguration();

    void start();

    void update();

    void shiftLeft();

    void shiftRight();

    void rotateClockwise();

    void rotateCounterClockwise();

    void hardDrop();

    void softDrop();

    void hold();

    void pause();

    boolean isPaused();

    boolean isGameOver();

    List<IMino> getAllMinos();

    List<IMino> getGhostMinos();

    GTromino getHoldGtrominoType();

    GTromino[] getPreviewGtrominos(int count);

    IReadableScore getScore();

    void saveHighScore(String playerName);

    void saveGameState();

    void loadGameState();
}
