package edu.dhbwka.gtris.model.application.entities.primitives;

import org.junit.jupiter.api.Assertions;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

public class MinoColorTest {

    @Test void testFromRGB() {
        MinoColor result = MinoColor.fromRGB(255, 255, 255, 255);
        int[] expected = { 255, 255, 255, 255 };
        assertArrayEquals(expected, result.getRgba());
    }

    @Test void testFromRGB1() {
        MinoColor result = MinoColor.fromRGB(0, 0, 0, 0);
        int[] expected = { 0, 0, 0, 0 };
        assertArrayEquals(expected, result.getRgba());
    }

    @Test void testFromRGB2() {
        MinoColor result = MinoColor.fromRGB(127, 100, 35, 200);
        int[] expected = { 127, 100, 35, 200 };
        assertArrayEquals(expected, result.getRgba());
    }

    @Test void testFromRGB3() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            MinoColor.fromRGB(256, 0, 0, 0);
        });
    }

    @Test void testEquals() {
        MinoColor result1 = MinoColor.fromRGB(255, 255, 255, 255);
        MinoColor result2 = MinoColor.fromRGB(255, 255, 255, 255);
        assertEquals(result1, result2);
    }
}
