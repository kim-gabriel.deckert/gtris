package edu.dhbwka.gtris.model.application.entities;

import edu.dhbwka.gtris.model.application.GTrominoFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class DroppingGtrominoTest {
    @Test void testRotateMatrixClockwise() {
        GTromino gtromino = GTrominoFactory.buildGTromino('L');
        DroppingGtromino droppingGTromino = new DroppingGtromino(gtromino);
        IMino[][] inputMatrix = gtromino.createMinos();
        IMino[][] outputMatrix =
                droppingGTromino.rotateMatrixClockwise(inputMatrix, gtromino.getDimension(), false);

        Assertions.assertEquals(inputMatrix[2][0], outputMatrix[0][0]);
        Assertions.assertEquals(inputMatrix[2][1], outputMatrix[1][0]);
        Assertions.assertEquals(inputMatrix[1][1], outputMatrix[1][1]);
        Assertions.assertEquals(inputMatrix[0][1], outputMatrix[1][2]);

    }

    @Test void testRotateMatrixCounterClockwise() {
        GTromino gtromino = GTrominoFactory.buildGTromino('L');
        DroppingGtromino droppingGTromino = new DroppingGtromino(gtromino);

        IMino[][] inputMatrix = gtromino.createMinos();
        IMino[][] outputMatrix =
                droppingGTromino.rotateMatrixCounterClockwise(inputMatrix, gtromino.getDimension(),
                        false);
        Assertions.assertEquals(inputMatrix[1][1], outputMatrix[1][1]);

        Assertions.assertEquals(inputMatrix[0][1], outputMatrix[1][0]);
        Assertions.assertEquals(inputMatrix[1][1], outputMatrix[1][1]);
        Assertions.assertEquals(inputMatrix[2][0], outputMatrix[2][2]);
        Assertions.assertEquals(inputMatrix[2][1], outputMatrix[1][2]);
    }
}
