package edu.dhbwka.gtris.model.application;

import edu.dhbwka.gtris.model.application.entities.GTromino;
import edu.dhbwka.gtris.model.application.entities.primitives.MinoColor;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.MockedConstruction;
import org.mockito.Mockito;
import static org.mockito.Mockito.when;

public class GTrominoFactoryTest {
    @Test public void testBuildGTrominoWithRegisteredType() {
        // GTromino<A> mockGTromino = (GTromino<A>) Mockito.mock(GTromino.class);
        MinoColor minoColor = MinoColor.fromRGB(127, 127, 127, 127);
        final MockedConstruction<A> gtrominoMock = Mockito.mockConstruction(A.class, (mock, context) -> {
            when(mock.getColor()).thenReturn(minoColor);
        });
        GTrominoFactory.register('A', () -> {return new A();});

        final GTromino gtromino = GTrominoFactory.buildGTromino('A');
        Assertions.assertEquals(1, gtrominoMock.constructed().size());
        Assertions.assertEquals(gtromino.getColor(), minoColor);
        gtrominoMock.close();
    }

    static class A extends GTromino {
        protected A() {
            super('A', null, null, 4, null);
        }
    }

}
