package edu.dhbwka.gtris.model.application;

import edu.dhbwka.gtris.model.application.entities.GTromino;
import java.util.concurrent.LinkedBlockingQueue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class GTrominoQueueTest {
    private GTrominoQueue queue;

    @BeforeEach void setupGTrominoFactory() {
        GTrominoFactory.registerDefaultGTrominos();
        this.queue = new GTrominoQueue(10,3);
    }

    @Test public void testFillWithGTrominosWithCapacityGreaterThanNumAvailableProducts() {

        LinkedBlockingQueue<GTromino> queue = new LinkedBlockingQueue<GTromino>();
        final int initialSize = this.queue.size();
        final int initialCapacity = this.queue.remainingCapacity();
        this.queue.fillWithGTrominos();
        assertTrue(this.queue.size() > initialSize);
        assertTrue(this.queue.remainingCapacity() < initialCapacity);
    }

    @Test public void testFillWithGTrominosWithCapacityEqualNumAvailableProducts() {
        while (this.queue.remainingCapacity() > GTrominoFactory.getNumAvailableProducts()) {
            try {
                this.queue.put(GTrominoFactory.buildGTromino('I'));
                this.queue.put(GTrominoFactory.buildGTromino('J'));
                this.queue.put(GTrominoFactory.buildGTromino('L'));
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }

        final int initialSize = this.queue.size();
        this.queue.fillWithGTrominos();
        assertEquals(this.queue.size(), initialSize);
    }

    @Test public void testPickGTrominoWhenSizeLessThanPreviewCountTimesTwo() {
        while (this.queue.size() > this.queue.getGTrominoPreviewCount() * 2) {
            this.queue.poll();
        }

        final GTromino initialPick = this.queue.getPreviewGTrominos(0);
        final GTromino pick = this.queue.pickGTromino();
        assertEquals(pick, initialPick);
    }

    @Test public void testGetPreviewGTrominosWithValidIndex() {
        assertNotEquals(this.queue.getPreviewGTrominos(0), null);
    }

    @Test public void testGetPreviewGTrominosWithInvalidIndex() {
        assertThrows(IndexOutOfBoundsException.class, () -> this.queue.getPreviewGTrominos(this.queue.size()));
    }
}
