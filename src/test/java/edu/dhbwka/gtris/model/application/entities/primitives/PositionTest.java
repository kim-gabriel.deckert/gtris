package edu.dhbwka.gtris.model.application.entities.primitives;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class PositionTest {

    @Test
    public void createRelativePosition_AdvanceBothCoordinates_ResultsInNewPosition() {
        // Arrange
        var originalPosition = new Position(10, 20);

        // Act
        var relativePosition = originalPosition.createRelativePosition(5, 10);

        // Assert
        assertEquals(15, relativePosition.getX());
        assertEquals(30, relativePosition.getY());
    }

    @Test
    public void createRelativePosition_AdvanceXCoordinateOnly_ResultsInNewPosition() {
        // Arrange
        var originalPosition = new Position(10, 20);

        // Act
        var relativePosition = originalPosition.createRelativePosition(5, 0);

        // Assert
        assertEquals(15, relativePosition.getX());
        assertEquals(20, relativePosition.getY());
    }

    @Test
    public void createRelativePosition_AdvanceYCoordinateOnly_ResultsInNewPosition() {
        // Arrange
        var originalPosition = new Position(10, 20);

        // Act
        var relativePosition = originalPosition.createRelativePosition(0, 10);

        // Assert
        assertEquals(10, relativePosition.getX());
        assertEquals(30, relativePosition.getY());
    }

    @Test
    public void createRelativePosition_NoAdvance_ResultsInOriginalPosition() {
        // Arrange
        var originalPosition = new Position(10, 20);

        // Act
        var relativePosition = originalPosition.createRelativePosition(0, 0);

        // Assert
        assertEquals(10, relativePosition.getX());
        assertEquals(20, relativePosition.getY());
    }
    @Test
    public void collides_SameCoordinates_ReturnsTrue() {
        // Arrange
        var position1 = new Position(10, 20);
        var position2 = new Position(10, 20);

        // Act
        var collides = position1.collides(position2);

        // Assert
        assertTrue(collides);
    }

    @Test
    public void collides_DifferentCoordinates_ReturnsFalse() {
        // Arrange
        var position1 = new Position(10, 20);
        var position2 = new Position(15, 25);

        // Act
        var collides = position1.collides(position2);

        // Assert
        assertFalse(collides);
    }
}
